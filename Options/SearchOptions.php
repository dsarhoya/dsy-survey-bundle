<?php

namespace DSYSurveyBundle\Options;

use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\Question;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Search Option.
 *
 * @property bool|null                              $boolValue
 * @property string|null                            $textValue
 * @property \DateTime|null                         $date
 * @property array|null                             $orderBy
 * @property string|null                            $type
 * @property Question|null                          $question
 * @property Question|null                          $newFlow
 * @property MultipleChoiceQuestionAlternative|null $alternative
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class SearchOptions extends BaseOptions
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'date' => null,
            'boolValue' => null,
            'textValue' => null,
            'orderBy' => null,
            'type' => null,
            'question' => null,
            'alternative' => null,
            'newFlow' => null,
        ]);

        $resolver
            ->setAllowedTypes('date', ['null', \DateTime::class])
            ->setAllowedTypes('boolValue', ['null', 'boolean'])
            ->setAllowedTypes('textValue', ['null', 'string'])
            ->setAllowedTypes('orderBy', ['null', 'array'])
            ->setAllowedTypes('type', ['null', 'string'])
            ->setAllowedTypes('question', ['null', Question::class])
            ->setAllowedTypes('newFlow', ['null', Question::class])
            ->setAllowedTypes('alternative', ['null', MultipleChoiceQuestionAlternative::class])
        ;
    }
}
