<?php

namespace DSYSurveyBundle\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of BaseOptions
 *
 * @author matias
 */
abstract class BaseOptions extends OptionsResolver{
    
    public $options;
    
    public function __construct(array $options = array())
    {
        $this->configureOptions($this);

        $this->options = $this->resolve($options);
    }
    
    public function __get($name) {
        return $this->options[$name];
    }
    
    abstract public function configureOptions(OptionsResolver $resolver);
}
