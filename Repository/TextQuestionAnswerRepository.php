<?php

namespace DSYSurveyBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DSYSurveyBundle\Entity\TextQuestionAnswer;
use Doctrine\ORM\QueryBuilder;
use DSYSurveyBundle\Options\SearchOptions;

/**
 * TextQuestionAnswerRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TextQuestionAnswerRepository extends ServiceEntityRepository
{
    /**
     * Constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TextQuestionAnswer::class);
    }

    /**
     * answers.
     *
     * @param array $optionsArray
     *
     * @return array
     */
    public function answers(array $optionsArray = []): array
    {
        return $this->answersQueryBuilder($optionsArray)->getQuery()->getResult();
    }

    /**
     * answers QueryBuilder.
     *
     * @param array $optionsArray
     *
     * @return QueryBuilder
     */
    public function answersQueryBuilder(array $optionsArray = []): QueryBuilder
    {
        $options = new SearchOptions($optionsArray);
        $qb = $this->createQueryBuilder('tqa');

        if (null !== $options->date) {
            $begin = clone $options->date;
            $begin->setTime(0, 0, 0);
            $end = clone $options->date;
            $end->setTime(23, 59, 59);

            $qb->andWhere($qb->expr()->between('tqa.createdAt', ':begin', ':end'))
                ->setParameter('end', $end)
                ->setParameter('begin', $begin);
        }

        if (null !== $options->textValue) {
            $qb->andWhere($qb->expr()->eq('tqa.textValue', ':value'))
                ->setParameter('value', $options->textValue);
        }

        if (null !== $options->orderBy) {
            foreach ($options->orderBy as $field => $direction) {
                $qb->addOrderBy($field, $direction);
            }
        } else {
            $qb->orderBy('bqa.createdAt', 'desc');
        }

        return $qb;
    }
}
