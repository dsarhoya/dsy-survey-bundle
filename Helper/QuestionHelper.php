<?php

namespace DSYSurveyBundle\Helper;

use DSYSurveyBundle\Entity\BooleanQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\TextQuestion;
use DSYSurveyBundle\Form\BooleanQuestionType;
use DSYSurveyBundle\Form\MultipleChoiceQuestionType;
use DSYSurveyBundle\Form\TextQuestionType;

class QuestionHelper
{
    /**
     * create Question by type of question.
     */
    public static function entityFactoryByQuestion(string $type): ? Question
    {
        switch ($type) {
            case Question::TYPE_TEXT:
                return new TextQuestion();
            case Question::TYPE_BOOLEAN:
                return new BooleanQuestion();
            case Question::TYPE_MULTIPLE_CHOICE:
            case Question::TYPE_SINGLE_CHOICE:
            case Question::TYPE_GROUP_CHOICE:
            case Question::TYPE_SINGLE_AUTOCOMPLETE:
                return new MultipleChoiceQuestion($type);
        }

        throw new \Exception('Type not implemented');
    }

    /**
     * get Form class by Question.
     */
    public static function getFormByQuestion(string $type): ? string
    {
        switch ($type) {
            case Question::TYPE_TEXT:
                return TextQuestionType::class;
            case Question::TYPE_BOOLEAN:
                return BooleanQuestionType::class;
            case Question::TYPE_MULTIPLE_CHOICE:
            case Question::TYPE_SINGLE_CHOICE:
            case Question::TYPE_GROUP_CHOICE:
            case Question::TYPE_SINGLE_AUTOCOMPLETE:
                return MultipleChoiceQuestionType::class;
        }

        throw new \Exception('Type not implemented');
    }
}
