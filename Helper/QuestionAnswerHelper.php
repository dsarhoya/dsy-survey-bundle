<?php

namespace DSYSurveyBundle\Helper;

use DSYSurveyBundle\Entity\BooleanQuestionAnswer;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\QuestionAnswer;
use DSYSurveyBundle\Entity\TextQuestionAnswer;
use DSYSurveyBundle\Form\BooleanQuestionAnswerType;
use DSYSurveyBundle\Form\MultipleChoiceQuestionAnswerType;
use DSYSurveyBundle\Form\TextQuestionAnswerType;

class QuestionAnswerHelper
{
    /**
     * create Question by type of question.
     *
     * @return QuestionAnswer|TextQuestionAnswer|BooleanQuestionAnswer|MultipleChoiceQuestionAnswer|null
     */
    public static function entityFactoryByQuestion(string $type): ? QuestionAnswer
    {
        $class = self::getAnswerClassByQuestion($type);

        return new $class();
    }

    /**
     * get Form class by Question.
     */
    public static function getFormByQuestion(string $type): ? string
    {
        switch ($type) {
            case Question::TYPE_TEXT:
                return TextQuestionAnswerType::class;
            case Question::TYPE_BOOLEAN:
                return BooleanQuestionAnswerType::class;
            case Question::TYPE_MULTIPLE_CHOICE:
            case Question::TYPE_SINGLE_CHOICE:
            case Question::TYPE_GROUP_CHOICE:
            case Question::TYPE_SINGLE_AUTOCOMPLETE:
                return MultipleChoiceQuestionAnswerType::class;
        }

        throw new \Exception('Type not implemented');
    }

    /**
     * get Form class by Question.
     */
    public static function getAnswerClassByQuestion(string $type): ? string
    {
        switch ($type) {
            case Question::TYPE_TEXT:
                return TextQuestionAnswer::class;
            case Question::TYPE_BOOLEAN:
                return BooleanQuestionAnswer::class;
            case Question::TYPE_MULTIPLE_CHOICE:
            case Question::TYPE_SINGLE_CHOICE:
            case Question::TYPE_GROUP_CHOICE:
            case Question::TYPE_SINGLE_AUTOCOMPLETE:
                return MultipleChoiceQuestionAnswer::class;
        }

        throw new \Exception('Type not implemented');
    }
}
