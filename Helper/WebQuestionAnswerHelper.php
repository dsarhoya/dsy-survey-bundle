<?php

namespace DSYSurveyBundle\Helper;

use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Form\Web\BooleanQuestionAnswerType;
use DSYSurveyBundle\Form\Web\MultipleChoiceQuestionAnswerType;
use DSYSurveyBundle\Form\Web\MultipleChoiceQuestionAnswerValueType;
use DSYSurveyBundle\Form\Web\TextQuestionAnswerType;

/**
 * WebQuestionAnswerHelper.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class WebQuestionAnswerHelper
{
    /**
     * get Form class by  on WEB.
     */
    public static function getFormByQuestion(string $type): ? string
    {
        switch ($type) {
            case Question::TYPE_TEXT:
                return TextQuestionAnswerType::class;
            case Question::TYPE_BOOLEAN:
                return BooleanQuestionAnswerType::class;
            case Question::TYPE_MULTIPLE_CHOICE:
            case Question::TYPE_SINGLE_CHOICE:
            case Question::TYPE_SINGLE_AUTOCOMPLETE:
                return MultipleChoiceQuestionAnswerType::class;
            case Question::TYPE_GROUP_CHOICE:
                return MultipleChoiceQuestionAnswerValueType::class;
        }

        throw new \Exception('Type not implemented');
    }
}
