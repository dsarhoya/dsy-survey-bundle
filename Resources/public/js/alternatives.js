$(function() {
  console.info("ALTERNATIVE AND VALUES JS");

  var $collectionHolderAlternative;
  $collectionHolderAlternative = $("#dsysurveybundle_question_alternatives");

  var $collectionHolderValues;
  $collectionHolderValues = $("#dsysurveybundle_question_values");

  var $addAlternativeButton = $(".js-add-alternative");
  var $addValueButton = $(".js-add-value");

  var indexAlt = $collectionHolderAlternative.find(">.row").length;
  var indexVal = $collectionHolderValues.find(">.row").length;

  $collectionHolderAlternative.data("index", indexAlt);
  $collectionHolderValues.data("index", indexVal);

  $addAlternativeButton.on("click", function(e) {
    e.preventDefault();
    addAlternativeForm($collectionHolderAlternative);
  });

  $addValueButton.on("click", function(e) {
    e.preventDefault();
    addValueForm($collectionHolderValues);
  });

  $(".js-delete-alternative").on("click", function(e) {
    e.preventDefault();
    removeAlternative(
      $(this)
        .parent()
        .parent(),
      $collectionHolderAlternative
    );
  });

  $(".js-delete-value").on("click", function(e) {
    e.preventDefault();
    removeValue(
      $(this)
        .parent()
        .parent(),
      $collectionHolderValues
    );
  });
});

function addAlternativeForm($collectionHolder) {
  var newForm = $collectionHolder.data("prototype");
  var index = $collectionHolder.data("index");
  newForm = newForm.replace(/__name__/g, index);
  $newForm = $(newForm);

  $collectionHolder.data("index", index + 1);
  $collectionHolder.append($newForm);
  $newForm.find(".js-delete-alternative").on("click", function(e) {
    e.preventDefault();
    removeAlternative(
      $(this)
        .parent()
        .parent(),
      $collectionHolder
    );
  });
}

function addValueForm($collectionHolder) {
  var newForm = $collectionHolder.data("prototype");
  var index = $collectionHolder.data("index");
  newForm = newForm.replace(/__name__/g, index);
  $newForm = $(newForm);

  $collectionHolder.data("index", index + 1);
  $collectionHolder.append($newForm);
  $newForm.find(".js-delete-value").on("click", function(e) {
    e.preventDefault();
    removeValue(
      $(this)
        .parent()
        .parent(),
      $collectionHolder
    );
  });
}

function removeAlternative($alternative, $collectionHolder) {
  var index = $collectionHolder.data("index");
  //   console.log(index);
  if (index <= 2) {
    return alert("Al menos debe ingresar dos alternativas.");
  }
  if (true === confirm("¿Desea eliminar esta alternativa?")) {
    $collectionHolder.data("index", index - 1);
    $alternative.remove();
  }
}

function removeValue($value, $collectionHolder) {
  var index = $collectionHolder.data("index");
  if (index <= 2) {
    return alert("Al menos debe ingresar dos valores.");
  }
  if (true === confirm("¿Desea eliminar este valor?")) {
    $collectionHolder.data("index", index - 1);
    $value.remove();
  }
}
