<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\Section;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SectionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nombre',
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('description', null, [
                'label' => 'Descripción',
            ])
            ->add('code', null, [
                'label' => 'Código', // Qué pasa si no ocupa código?
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('enabled', null, [
                'label' => 'Habilitada',
            ])
            ->add('mandatory', ChoiceType::class, [
                'label' => 'Requerido',
                'choices' => [
                    'Si' => 1,
                    'No' => 0,
                ],
            ])
        ;

        $buttons = [
            'cancel_link' => $options['cancel_link'],
        ];

        if (null !== $options['pages_link']) {
            $buttons['extra_buttons'] = [
                ['verPaginas', LinkType::class, ['link' => $options['pages_link']]],
            ];
        }

        $builder
            ->add('buttons', ButtonsGroupsType::class, $buttons)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Section::class,
            'pages_link' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_section';
    }
}
