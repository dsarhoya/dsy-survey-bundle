<?php

namespace DSYSurveyBundle\Form\Type;

use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionValue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GroupChoice Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class GroupChoiceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var MultipleChoiceQuestionAlternative $alt */
        foreach ($options['alternatives'] as $alt) {
            $builder
                ->add($alt->getId(), EntityType::class, [
                    'label' => $alt->getTitle(),
                    'class' => MultipleChoiceQuestionValue::class,
                    'choices' => $options['values'],
                    'choice_label' => false,
                    'expanded' => true,
                ])
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['values'] = $options['values'];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('alternatives')
            ->setRequired('values')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'group_choice';
    }
}
