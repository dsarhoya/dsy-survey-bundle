<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * WEB MultipleChoiceAlternative Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class MultipleChoiceAlternativeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Texto',
                'attr' => [
                    'placeholder' => 'Ingrese el texto a mostrar',
                    'maxlength' => 255,
                ],
            ])
            ->add('code', null, [
                'label' => 'Código',
                'attr' => [
                    'placeholder' => 'Ingrese el código de la alternativa',
                    'maxlength' => 255,
                ],
            ])
            ->add('enabled', null, [
                'label' => 'Habilitada',
            ])
            // ->add('delete', ButtonType::class, [
            //     'attr' => ['class' => 'btn-danger js-delete-alternative'],
            //     'label' => 'Borrar',
            // ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MultipleChoiceQuestionAlternative::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_question_alternative';
    }
}
