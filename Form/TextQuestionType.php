<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\TextQuestion;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TextQuestionType extends QuestionType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => TextQuestion::class,
        ));
    }
}
