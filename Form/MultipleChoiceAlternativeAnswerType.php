<?php

namespace DSYSurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionValue;

/**
 * MultipleChoiceAlternativeAnswerType Type.
 *
 * @author mati
 */
class MultipleChoiceAlternativeAnswerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alternative_id', EntityType::class, [
                'class' => MultipleChoiceQuestionAlternative::class,
                'property_path' => 'alternative',
                'required' => true,
            ])
            ->add('value_id', EntityType::class, [
                'class' => MultipleChoiceQuestionValue::class,
                'property_path' => 'value',
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MultipleChoiceAlternativeAnswer::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
