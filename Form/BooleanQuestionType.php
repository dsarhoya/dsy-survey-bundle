<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\BooleanQuestion;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BooleanQuestionType extends QuestionType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => BooleanQuestion::class
        ));
    }
}
