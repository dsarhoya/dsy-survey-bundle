<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\TextQuestionAnswer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * TextQuestionAnswer Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class TextQuestionAnswerType extends QuestionAnswerType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        /** @var TextQuestionAnswer $questionAnswer */
        $questionAnswer = $builder->getData();
        $question = $questionAnswer->getQuestion();

        $constraints = $question->getMandatory() ? [new NotBlank()] : [];

        $builder
            ->add('value', TextType::class, [
                'constraints' => $constraints,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => TextQuestionAnswer::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
