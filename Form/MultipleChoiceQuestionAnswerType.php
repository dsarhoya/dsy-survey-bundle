<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\QuestionAnswer;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Count;

/**
 * MultipleChoiceQuestionAnswerType Type.
 *
 * @author mati
 */
class MultipleChoiceQuestionAnswerType extends QuestionAnswerType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        /** @var MultipleChoiceQuestionAnswer $questionAnswer */
        $questionAnswer = $builder->getData();
        /** @var MultipleChoiceQuestion $question */
        $question = $questionAnswer->getQuestion();

        $constraints = [];
        if (Question::TYPE_MULTIPLE_CHOICE === $question->getType() && ($question->getMin() || $question->getMax())) {
            $constraints = [
                    new Count([
                        'max' => $question->getMax(),
                        'min' => $question->getMin(),
                        'minMessage' => 'Debe seleccionar al menos {{ limit }} alternativa(s).',
                        'maxMessage' => 'Debe seleccionar máximo {{ limit }} alternativa(s).',
                    ]),
                ];
        }

        /*
         * Mati: esto se hizo porque cambió el nombre de la propiedad que trae los alternative answers. Con este evento
         * podemos seguir soportando los dos casos.
         *
         * Eventualmente, podemos sacarlo y solo dejar "alternative_answers" ya que ese es el correcto.
         */
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($constraints) {
            $data = $event->getData();
            $form = $event->getForm();

            if (!$data) {
                return;
            }

            $path = 'alternative_answers';

            if (isset($data['alternatives'])) {
                $path = 'alternatives';
            }

            $form->add($path, CollectionType::class, [
                'property_path' => 'alternativeAnswers',
                'entry_type' => MultipleChoiceAlternativeAnswerType::class,
                'allow_add' => true,
                'by_reference' => false,
                'entry_options' => [],
                'constraints' => $constraints,
            ]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => MultipleChoiceQuestionAnswer::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
