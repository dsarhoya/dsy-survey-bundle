<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\SurveyInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nombre',
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('description', null, [
                'label' => 'Descripción',
            ])
            ->add('code', null, [
                'label' => 'Código', // Qué pasa si no ocupa código?
            ])
            ->add('enabled', null, [
                'label' => 'Habilitada',
            ])
        ;

        $buttons = [
            'cancel_link' => $options['cancel_link'],
        ];

        if ($options['sections_link']) {
            $buttons['extra_buttons'] = [
                ['verSecciones', LinkType::class, ['link' => $options['sections_link']]],
            ];
        }

        $builder
            ->add('buttons', ButtonsGroupsType::class, $buttons)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => SurveyInterface::class,
            'sections_link' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_survey';
    }
}
