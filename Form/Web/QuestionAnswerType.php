<?php

namespace DSYSurveyBundle\Form\Web;

use DSYSurveyBundle\Event\DSYSurveyBundleEvents;
use DSYSurveyBundle\Event\GetCustomConstraintQuestionAnswerEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * WEB QuestionAnswer Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
abstract class QuestionAnswerType extends AbstractType
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * Constructor.
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', TextType::class, [
                'required' => false,
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('imageKey', TextType::class, [
                'required' => false,
            ])
            ->add('images', CollectionType::class, [
                'entry_type' => QuestionAnswerImageType::class,
                'allow_add' => true,
                'by_reference' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'questions' => [],
        ]);

        $event = new GetCustomConstraintQuestionAnswerEvent();
        $this->eventDispatcher->dispatch($event, DSYSurveyBundleEvents::GET_CUSTOM_CONSTRAINTS_QUESTION_ANSWER);

        if (count($event->getConstraints())) {
            $resolver->setDefault('constraints', function (Options $options) use ($event) {
                return $event->getConstraints();
            });
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_question_answer';
    }
}
