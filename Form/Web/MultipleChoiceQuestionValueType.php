<?php

namespace DSYSurveyBundle\Form\Web;

use DSYSurveyBundle\Entity\MultipleChoiceQuestionValue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

/**
 * WEB MultipleChoiceQuestionValue Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class MultipleChoiceQuestionValueType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Texto',
                'attr' => [
                    'placeholder' => 'Ingrese el texto a mostrar',
                ],
            ])
            ->add('position', null, [
                'label' => 'Orden',
            ])
            ->add('enabled', null, [
                'label' => 'Habilitada',
            ])
            // ->add('delete', ButtonType::class, [
            //     'attr' => ['class' => 'btn-danger btn-sm js-delete-value'],
            //     'label' => 'Borrar',
            // ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MultipleChoiceQuestionValue::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_question_value';
    }
}
