<?php

namespace DSYSurveyBundle\Form\Web;

use DSYSurveyBundle\Entity\FlowException;
use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Form\ButtonsGroupsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * FlowException Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class FlowExceptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var FlowException $exception */
        $exception = $builder->getData();
        $builder
            ->add('newFlow', EntityType::class, [
                'label' => 'Siguiente pregunta',
                'class' => Question::class,
                'choices' => $options['questions'],
                'choice_label' => function (Question $choice) {
                    return 'Página '.$choice->getPage()->getCode().', Pregunta "'.(null !== $choice->getCode() ? $choice->getCode() : $choice->getTitle()).'"';
                },
                'placeholder' => 'Pregunta Final',
                'required' => false,
                'group_by' => function (Question $choice) {
                    return $choice->getPage()->getSection()->getName();
                },
            ]);

        $question = $exception->getQuestion();
        if ($question instanceof MultipleChoiceQuestion && in_array($question->getType(), [Question::TYPE_SINGLE_CHOICE/* , Question::TYPE_SINGLE_AUTOCOMPLETE */])) {
            $builder
                ->add('alternative', EntityType::class, [
                    'label' => 'Alternativa',
                    'placeholder' => 'Todas',
                    'required' => false,
                    'class' => MultipleChoiceQuestionAlternative::class,
                    'choices' => $question->getAlternativesEnabled(),
                    'choice_label' => 'title',
                ]);
        }

        $builder->add('buttons', ButtonsGroupsType::class, [
            'cancel_link' => $options['cancel_link'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FlowException::class,
        ])->setRequired('questions')->setRequired('cancel_link');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_flow_exception';
    }
}
