<?php

namespace DSYSurveyBundle\Form\Web;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use DSYSurveyBundle\Entity\TextQuestionAnswer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * TextQuestionAnswer Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class TextQuestionAnswerType extends QuestionAnswerType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('value', TextareaType::class, [
                'required' => true,
                'label' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => TextQuestionAnswer::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_text_question_answer';
    }
}
