<?php

namespace DSYSurveyBundle\Form\Web;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use DSYSurveyBundle\Entity\BooleanQuestionAnswer;

/**
 * WEB BooleanQuestionAnswer Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class BooleanQuestionAnswerType extends QuestionAnswerType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('value', CheckboxType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => BooleanQuestionAnswer::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_boolean_question_answer';
    }
}
