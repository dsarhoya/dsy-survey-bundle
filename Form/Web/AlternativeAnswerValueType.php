<?php

namespace DSYSurveyBundle\Form\Web;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionValue;

/**
 * AlternativeAnswerValueType.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class AlternativeAnswerValueType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value', EntityType::class, [
                'label' => false,
                'expanded' => true,
                'class' => MultipleChoiceQuestionValue::class,
                'choices' => $options['values'],
                'choice_label' => 'title',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('values')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_question_alternative_answer_value';
    }
}
