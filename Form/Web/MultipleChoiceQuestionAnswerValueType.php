<?php

namespace DSYSurveyBundle\Form\Web;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer;
use DSYSurveyBundle\Form\DataTransformer\AlternativeValueToAlternativeAnswerValueTransformer;
use DSYSurveyBundle\Form\Type\GroupChoiceType;

/**
 * WEB MultipleChoiceQuestionAnswerValue Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class MultipleChoiceQuestionAnswerValueType extends QuestionAnswerType
{
    /**
     * @var AlternativeValueToAlternativeAnswerValueTransformer
     */
    protected $transformer;

    /**
     * Constructor.
     *
     * @param AlternativeValueToAlternativeAnswerValueTransformer $transformer
     */
    public function __construct(AlternativeValueToAlternativeAnswerValueTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alternatives', GroupChoiceType::class, [
                'label' => false,
                'alternatives' => $options['alternatives'],
                'values' => $options['values'],
            ])
        ;

        $builder
            ->get('alternatives')
            ->addModelTransformer($this->transformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
        ->setRequired('alternatives')
        ->setRequired('values')
        ->setDefaults([
            'data_class' => MultipleChoiceQuestionAnswer::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_choice_question_answer';
    }
}
