<?php

namespace DSYSurveyBundle\Form\Web;

use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Form\DataTransformer\AlternativeToAlternativeAnswerTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;

/**
 * WEB MultipleChoiceQuestionAnswerType Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class MultipleChoiceQuestionAnswerType extends QuestionAnswerType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        /** @var MultipleChoiceQuestionAnswer $questionAnswer */
        $questionAnswer = $builder->getData();
        /** @var MultipleChoiceQuestion $question */
        $question = $questionAnswer->getQuestion();

        $constraints = [];
        if (Question::TYPE_MULTIPLE_CHOICE === $question->getType() && ($question->getMin() || $question->getMax())) {
            $constraints = [
                new Count([
                    'max' => $question->getMax(),
                    'min' => $question->getMin(),
                    'minMessage' => 'Debe seleccionar al menos {{ limit }} alternativa(s).',
                    'maxMessage' => 'Debe seleccionar máximo {{ limit }} alternativa(s).',
                ]),
            ];
        }

        $alternativesTypeOptions = [
            'label' => false,
            'class' => MultipleChoiceQuestionAlternative::class,
            'choice_label' => 'title',
            'expanded' => true,
            'multiple' => Question::TYPE_MULTIPLE_CHOICE === $questionAnswer->getQuestion()->getType(),
            'choices' => $options['alternatives'],
            'constraints' => $constraints,
        ];

        if (Question::TYPE_SINGLE_AUTOCOMPLETE === $question->getType()) {
            $alternativesTypeOptions['expanded'] = false;
            $alternativesTypeOptions['placeholder'] = 'Seleccione...';
            $alternativesTypeOptions['attr'] = [
                'data-toggle' => 'select',
            ];
        }

        $builder
            ->add('alternatives', EntityType::class, $alternativesTypeOptions);

        $builder
            ->get('alternatives')
            ->addModelTransformer(new AlternativeToAlternativeAnswerTransformer($questionAnswer))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver
        ->setRequired('alternatives')
        ->setDefaults([
            'data_class' => MultipleChoiceQuestionAnswer::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_choice_question_answer';
    }
}
