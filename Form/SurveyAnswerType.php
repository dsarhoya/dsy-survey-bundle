<?php

namespace DSYSurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * SurveyAnswerType.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class SurveyAnswerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('survey_id', EntityType::class, [
                'class' => $options['survey_class'],
                'property_path' => 'survey',
            ])
            // ->add('answers', ArrayType, [
            //     'required' => true,
            //     'mapped' => false,
            //     'constraints' => [
            //         new NotNull(),
            //     ],
            // ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('survey_class')
            ->setDefaults([
                'allow_extra_fields' => true,
                'csrf_protection' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
