<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\Page;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Título',
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('code', null, [
                'label' => 'Código', // Qué pasa si no ocupa código?
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('enabled', null, [
                'label' => 'Habilitada',
            ])
        ;
        $buttons = [
            'cancel_link' => $options['cancel_link'],
        ];

        if (null !== $options['questions_link']) {
            $buttons['extra_buttons'] = [
                ['verPreguntas', LinkType::class, ['link' => $options['questions_link']]],
            ];
        }

        $builder
            ->add('buttons', ButtonsGroupsType::class, $buttons)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Page::class,
            'questions_link' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_page';
    }
}
