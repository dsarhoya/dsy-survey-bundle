<?php

namespace DSYSurveyBundle\Form;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ButtonsExtension extends AbstractTypeExtension
{
    public function getExtendedType()
    {
        // use FormType::class to modify (nearly) every field in the system
        return FormType::class;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cancel_link' => null,
        ));
    }
}
