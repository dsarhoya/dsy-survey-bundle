<?php

namespace DSYSurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ButtonsGroupsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['add_submit']) {
            $builder->add(...$options['submit_button']);
        }
        if ($options['cancel_link']) {
            $args = $options['cancel_button'];
            $args[] = ['link' => $options['cancel_link']];
            $builder->add(...$args);
        }
        foreach ($options['extra_buttons'] as $button) {
            $builder->add(...$button);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'mapped' => false,
            'label' => false,
            'add_submit' => true,
            'submit_button' => ['submit', SaveType::class, ['label' => 'Guardar']],
            'cancel_link' => false,
            'cancel_button' => ['cancelar', CancelType::class],
            'extra_buttons' => [],
        ));
    }
}
