<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionValue;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Form\Web\MultipleChoiceQuestionValueType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * WEB MultipleChoiceQuestionType.
 */
class MultipleChoiceQuestionType extends QuestionType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        /** @var MultipleChoiceQuestion $question */
        $question = $builder->getData();

        if (Question::TYPE_MULTIPLE_CHOICE === $question->getType()) {
            $builder
                    ->add('min', IntegerType::class, [
                        'label' => 'Cantidad mínima de alternativas',
                    ])
                    ->add('max', IntegerType::class, [
                        'label' => 'Cantidad máxima de alternativas',
                    ])
                ;
        }

        if (Question::TYPE_SINGLE_AUTOCOMPLETE === $question->getType()) {
            $builder->add('alternativesFile', FileType::class, [
                'label' => 'Archivo con alternativas',
                'mapped' => false,
            ]);
        } else {
            $builder
                ->add('alternatives', CollectionType::class, [
                    'entry_type' => MultipleChoiceAlternativeType::class,
                    'entry_options' => [
                        'label' => false,
                    ],
                    'delete_empty' => function (MultipleChoiceQuestionAlternative $alt = null) {
                        return null === $alt || empty($alt->getTitle());
                    },
                    'allow_add' => true,
                    'by_reference' => false,
                    'error_bubbling' => false,
                    'label' => 'Alternativas',
                ]);
        }

        if (Question::TYPE_GROUP_CHOICE === $question->getType()) {
            $builder
                    ->add('values', CollectionType::class, [
                        'entry_type' => MultipleChoiceQuestionValueType::class,
                        'entry_options' => [
                            'label' => false,
                        ],
                        'delete_empty' => function (MultipleChoiceQuestionValue $val = null) {
                            return null === $val || empty($val->getTitle());
                        },
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false,
                        'error_bubbling' => false,
                        'label' => 'Valores',
                    ])
                ;
        }

        if ($builder->has('buttons')) {
            /** @var FormBuilderInterface|null $buttons */
            $buttons = $builder->get('buttons');
            $options = $buttons->getOptions();

            $extras = [];
            if (Question::TYPE_SINGLE_AUTOCOMPLETE !== $question->getType()) {
                $extras = [['add_alternative', ButtonType::class, [
                    'attr' => ['class' => 'btn-info js-add-alternative'],
                    'label' => 'Nueva alternativa',
                ]]];
            }

            if (Question::TYPE_GROUP_CHOICE == $question->getType()) {
                $extras = array_merge($extras, [['add_value', ButtonType::class, [
                    'attr' => ['class' => 'btn-info js-add-value'],
                    'label' => 'Nuevo Valor',
                ]]]);
            }
            $options = array_merge($options, ['extra_buttons' => $extras]);
            $builder->remove('buttons');
            $builder->add($buttons->getName(), get_class($buttons->getType()->getInnerType()), $options);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MultipleChoiceQuestion::class,
            'type' => null,
            // 'csrf_protection' => false,
        ]);
    }
}
