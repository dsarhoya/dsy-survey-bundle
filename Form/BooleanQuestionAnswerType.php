<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\BooleanQuestionAnswer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * BooleanQuestionAnswer Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class BooleanQuestionAnswerType extends QuestionAnswerType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        /** @var BooleanQuestionAnswer $questionAnswer */
        $questionAnswer = $builder->getData();
        $question = $questionAnswer->getQuestion();

        $constraints = $question->getMandatory() ? [new NotBlank()] : [];

        $builder->add('value', CheckboxType::class, [
            'constraints' => $constraints,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => BooleanQuestionAnswer::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
