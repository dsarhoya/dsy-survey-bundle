<?php

namespace DSYSurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * WEB QuestionType.
 */
abstract class QuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Título',
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('code', null, [
                'label' => 'Código', // Qué pasa si no ocupa código?
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('description', null, [
                'required' => false,
            ])
            ->add('position', null, [
                'label' => 'Orden',
                'required' => false,
            ])
            ->add('enabled', null, [
                'label' => 'Habilitada',
            ])
            ->add('mandatory', ChoiceType::class, [
                'label' => 'Requerido',
                'choices' => [
                    'Si' => 1,
                    'No' => 0,
                ],
            ])
            ->add('hasComments', null, [
                'label' => 'Con Comentarios?',
            ])
            ->add('hasImages', null, [
                'label' => 'Con Imagenes?',
            ])
            ->add('buttons', ButtonsGroupsType::class, [
                'cancel_link' => $options['cancel_link'],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'dsysurveybundle_question';
    }
}
