<?php

namespace DSYSurveyBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer;
use DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use Doctrine\Common\Collections\Collection;

/**
 * AlternativeToAlternativeAnswer Transformer.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class AlternativeToAlternativeAnswerTransformer implements DataTransformerInterface
{
    /**
     * @var MultipleChoiceQuestionAnswer
     */
    private $questionAnswer;

    /**
     * Constructor.
     *
     * @param MultipleChoiceQuestionAnswer $questionAnswer
     */
    public function __construct(MultipleChoiceQuestionAnswer $questionAnswer)
    {
        $this->questionAnswer = $questionAnswer;
    }

    /**
     * @param Collection $alternativeAnswers
     *
     * @return array
     */
    public function transform($value)
    {
        if (!$value instanceof Collection) {
            return [];
        }

        return $value->map(function (MultipleChoiceAlternativeAnswer $aa) {
            return $aa->getAlternative();
        })->toArray();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param array|MultipleChoiceQuestionAlternative|null $altsAsArray
     *
     * @return array MultipleChoiceAlternativeAnswer[]
     */
    public function reverseTransform($value)
    {
        if (null === $value) {
            return [];
        }

        if (!is_array($value)) {
            $value = [$value];
        }
        $alternativeAnswers = [];
        /** @var MultipleChoiceQuestionAlternative $alt */
        foreach ($value as $alt) {
            $aa = new MultipleChoiceAlternativeAnswer();
            $aa->setAlternative($alt);
            if (!$this->questionAnswer->getAlternativeAnswers()->contains($aa)) {
                $alternativeAnswers[] = $aa;
            }
        }

        return $alternativeAnswers;
    }
}
