<?php

namespace DSYSurveyBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionValue;
use DSYSurveyBundle\Repository\MultipleChoiceQuestionAlternativeRepository;

/**
 * AlternativeToAlternativeAnswerValue Transformer.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class AlternativeValueToAlternativeAnswerValueTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Collection $alternativeAnswers
     *
     * @return array
     */
    public function transform($collection)
    {
        if (!$collection instanceof Collection) {
            return [];
        }

        return array_reduce($collection->toArray(), function ($carry, MultipleChoiceAlternativeAnswer $aa) {
            return array_merge($carry, [$aa->getAlternative()->getId() => $aa->getValue()->getId()]);
        }, []);
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param array|MultipleChoiceQuestionAlternative|null $altsAsArray
     *
     * @return array MultipleChoiceAlternativeAnswer[]
     */
    public function reverseTransform($values)
    {
        if (null === $values) {
            return [];
        }
        $alternativeAnswerValues = [];

        foreach ($values as $altId => $value) {
            if (null === $value || !$value instanceof MultipleChoiceQuestionValue) {
                continue;
            }

            /** @var MultipleChoiceQuestionAlternativeRepository $repoAlt */
            $repoAlt = $this->em->getRepository(MultipleChoiceQuestionAlternative::class);

            /** @var MultipleChoiceQuestionAlternative $alternative */
            $alternative = $repoAlt->find($altId);

            if (null === $alternative) {
                throw new \Exception('Error reversando dato alternativeAnswerValue, entidad no encontrada');
            }

            $aa = new MultipleChoiceAlternativeAnswer();
            $aa->setAlternative($alternative);
            $aa->setValue($value);
            $alternativeAnswerValues[] = $aa;
        }

        return $alternativeAnswerValues;
    }
}
