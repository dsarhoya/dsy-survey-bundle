<?php

namespace DSYSurveyBundle\Form;

use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Event\DSYSurveyBundleEvents;
use DSYSurveyBundle\Event\GetCustomConstraintQuestionAnswerEvent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * QuestionAnswer Type.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
abstract class QuestionAnswerType extends AbstractType
{
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question_id', EntityType::class, [
                'class' => Question::class,
                'property_path' => 'question',
                'required' => true,
                'choices' => $options['questions'],
            ])
            ->add('comment', TextType::class, [
                'required' => false,
            ])
            ->add('image_key', TextType::class, [
                'property_path' => 'imageKey',
                'required' => false,
            ])
            ->add('images', CollectionType::class, [
                'entry_type' => QuestionAnswerImageType::class,
                'allow_add' => true,
                'by_reference' => false,
                'entry_options' => [],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'allow_extra_fields' => true,
            'questions' => [],
        ]);

        $event = new GetCustomConstraintQuestionAnswerEvent();
        $this->eventDispatcher->dispatch($event, DSYSurveyBundleEvents::GET_CUSTOM_CONSTRAINTS_QUESTION_ANSWER);

        if (count($event->getConstraints())) {
            $resolver->setDefault('constraints', function (Options $options) use ($event) {
                return $event->getConstraints();
            });
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
