<?php

namespace DSYSurveyBundle\Entity;

interface EntityEnabledInterface
{
    public function isEnabled(): bool;
}
