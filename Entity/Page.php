<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Page Entity.
 *
 * @ORM\Table(name="srv_page")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\PageRepository")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class Page implements EntityEnabledInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"page_list", "page_detail"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @JMS\SerializedName("title")
     * @JMS\Groups({"page_list", "page_detail"})
     */
    protected $title;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("code")
     * @JMS\Groups({"page_detail"})
     */
    protected $code;

    /**
     * @var Section
     *
     * @ORM\ManyToOne(targetEntity="Section", inversedBy="pages")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id")
     * @JMS\SerializedName("section")
     * @JMS\Groups({"r_page_section"})
     */
    protected $section;

    /**
     * @var Collection Question[]
     *
     * @ORM\OneToMany(targetEntity="Question", mappedBy="page", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $questions;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\SerializedName("title")
     * @JMS\Groups({"page_detail"})
     */
    protected $html;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("vide")
     * @JMS\Groups({"page_detail"})
     */
    protected $video;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("created_at")
     * @JMS\Groups({"page_detail"})
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("updated_at")
     * @JMS\Groups({"page_detail"})
     */
    protected $updatedAt;

    /*
     * Entity Enabled property
     */
    use \DSYSurveyBundle\Traits\EntityEnabledTrait;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set html.
     *
     * @param string|null $html
     *
     * @return Page
     */
    public function setHtml($html = null)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get html.
     *
     * @return string|null
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Set video.
     *
     * @param string|null $video
     *
     * @return Page
     */
    public function setVideo($video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video.
     *
     * @return string|null
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Page
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Page
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set section.
     *
     * @param \DSYSurveyBundle\Entity\Section|null $section
     *
     * @return Page
     */
    public function setSection(\DSYSurveyBundle\Entity\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section.
     *
     * @return \DSYSurveyBundle\Entity\Section|null
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Add question.
     *
     * @param \DSYSurveyBundle\Entity\Question $question
     *
     * @return Page
     */
    public function addQuestion(\DSYSurveyBundle\Entity\Question $question)
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setPage($this);
        }

        return $this;
    }

    /**
     * Remove question.
     *
     * @param \DSYSurveyBundle\Entity\Question $question
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeQuestion(\DSYSurveyBundle\Entity\Question $question)
    {
        return $this->questions->removeElement($question);
    }

    /**
     * Get questions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Get questions.
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("questions")
     * @JMS\Groups({"r_page_question"})
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnabledQuestions()
    {
        $enabled = $this->questions->filter(function (EntityEnabledInterface $entity) {
            return true === $entity->isEnabled();
        })->getValues();

        return new ArrayCollection($enabled);
    }

    /**
     * get section id.
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("section_id")
     * @JMS\Groups({"page_detail"}).
     *
     * @return int|null
     */
    public function getSectionId(): ?int
    {
        return null !== $this->section ? $this->section->getId() : null;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Page
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }
}
