<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Description of TextQuestion.
 *
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\TextQuestionRepository")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class TextQuestion extends Question
{
    const TYPE_TEXT = 'text';
    const TYPE_NUMBER = 'number';

    /**
     * @var Collection TextQuestionAnswer[]
     *
     * @ORM\OneToMany(targetEntity="TextQuestionAnswer", mappedBy="question")
     */
    protected $answers;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("input_type")
     * @JMS\Groups({"question_list","question_detail"})
     */
    protected $inputType;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
        $this->inputType = self::TYPE_TEXT;
    }

    /**
     * Add answers.
     *
     * @return TextQuestion
     */
    public function addAnswer(TextQuestionAnswer $answers)
    {
        if (!$this->answers->contains($answers)) {
            $this->answers[] = $answers;
            $answers->setQuestion($this);
        }

        return $this;
    }

    /**
     * Remove answers.
     */
    public function removeAnswer(TextQuestionAnswer $answers)
    {
        $this->answers->removeElement($answers);
    }

    /**
     * Get answers.
     *
     * @return Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Get getQuestionAnswers.
     *
     * @return Collection
     */
    public function getQuestionAnswers()
    {
        return $this->answers;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("type")
     * @JMS\Groups({"question_list","question_detail"})
     *
     * @return string
     */
    public function getType()
    {
        return Question::TYPE_TEXT;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("discr")
     * @JMS\Groups({"question_list","question_detail"})
     *
     * @return string
     */
    public function getDiscriminatorColumn()
    {
        return $this->getType();
    }

    /**
     * Get the value of inputType.
     *
     * @return string
     */
    public function getInputType()
    {
        return $this->inputType;
    }

    /**
     * Set the value of inputType.
     *
     * @return self
     */
    public function setInputType($inputType)
    {
        $this->inputType = $inputType;

        return $this;
    }
}
