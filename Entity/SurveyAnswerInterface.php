<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * @author snake77se at dsarhoya.cl
 */
interface SurveyAnswerInterface
{
    /**
     * Get ID Survey.
     *
     * @return int
     */
    public function getId();

    /**
     * Get created at Survey.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Get updated at Survey.
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Get Survey to SurveyAnswer.
     *
     * @return SurveyInterface
     */
    public function getSurvey();

    /**
     * set survey at SurveyAnswer.
     *
     * @param SurveyInterface|null $survey
     *
     * @return SurveyAnswerInterface
     */
    public function setSurvey(SurveyInterface $survey = null);

    /**
     * Get Question Answer to SurveyAnswer.
     *
     * @return Collection
     */
    public function getQuestionAnswers();

    /**
     * add QuestionAnswer.
     *
     * @param QuestionAnswer $questionAnswer
     */
    public function addQuestionAnswer(QuestionAnswer $questionAnswer);

    /**
     * remove QuestionAnswer.
     *
     * @param QuestionAnswer $questionAnswer
     */
    public function removeQuestionAnswer(QuestionAnswer $questionAnswer);
}
