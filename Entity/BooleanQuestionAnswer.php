<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BooleanQuestionAnswer entity.
 *
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\BooleanQuestionAnswerRepository")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class BooleanQuestionAnswer extends QuestionAnswer
{
    /**
     * @var bool|null
     *
     * @ORM\Column(name="bool_value", type="boolean", nullable=true)
     * @JMS\SerializedName("value")
     * @JMS\Groups({"question_answer_detail"})
     */
    protected $boolValue;

    /**
     * @var BooleanQuestion
     *
     * @ORM\ManyToOne(targetEntity="BooleanQuestion", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * @Assert\NotNull()
     * @JMS\SerializedName("question")
     * @JMS\Groups({"r_question_answer_question", "r_boolean_question_answer_boolean_question"})
     */
    protected $question;

    /**
     * Set value.
     *
     * @param bool $boolValue
     *
     * @return BooleanQuestionAnswer
     */
    public function setValue($value)
    {
        $this->boolValue = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return bool
     */
    public function getValue()
    {
        return $this->boolValue;
    }

    /**
     * Set question.
     *
     * @param Question|null $question
     *
     * @return BooleanQuestionAnswer
     */
    public function setQuestion(Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return Question|null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("type")
     * @JMS\Groups({"question_answer_list","question_answer_detail"})
     *
     * @return string
     */
    public function getType()
    {
        return Question::TYPE_BOOLEAN;
    }

    /**
     * Get Question ID.
     *
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("question_id")
     * @JMS\Groups({"question_answer_detail"})
     *
     * @return int
     */
    public function getQuestionId()
    {
        return null !== $this->getQuestion() ? $this->getQuestion()->getId() : null;
    }

    /**
     * {@inheritdoc}
     */
    public function isAnswered()
    {
        return null !== $this->boolValue;
    }
}
