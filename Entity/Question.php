<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of Question.
 *
 * @ORM\Table(name="srv_question")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\QuestionRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "question" = "Question",
 *     "text" = "TextQuestion",
 *     "bool" = "BooleanQuestion",
 *     "multiplechoice" = "MultipleChoiceQuestion",
 * })
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class Question implements EntityEnabledInterface
{
    const TYPE_TEXT = 'text';
    const TYPE_BOOLEAN = 'bool';
    const TYPE_MULTIPLE_CHOICE = 'multiplechoice';
    const TYPE_SINGLE_CHOICE = 'singlechoice';
    const TYPE_GROUP_CHOICE = 'groupchoice';
    const TYPE_SINGLE_AUTOCOMPLETE = 'singleautocomplete';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"question_list", "question_detail"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     * @Assert\NotBlank()
     * @JMS\SerializedName("title")
     * @JMS\Groups({"question_list", "question_detail"})
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\SerializedName("description")
     * @JMS\Groups({"question_detail"})
     */
    protected $description;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("code")
     * @JMS\Groups({"question_detail"})
     */
    protected $code;

    /**
     * @var bool
     *
     * @ORM\Column(name="mandatory", type="boolean", options={"default":true})
     * @JMS\SerializedName("mandatory")
     * @JMS\Groups({"question_detail"})
     */
    protected $mandatory = true;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     * @JMS\SerializedName("position")
     * @JMS\Groups({"question_detail"})
     */
    protected $position;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("created_at")
     * @JMS\Groups({"question_detail"})
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("updated_at")
     * @JMS\Groups({"question_detail"})
     */
    protected $updatedAt;

    /**
     * @var Page
     *
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="questions")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     * @Gedmo\SortableGroup
     * @JMS\SerializedName("page")
     * @JMS\Groups({"r_question_page"})
     */
    protected $page;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": 1})
     * @JMS\SerializedName("enabled")
     * @JMS\Groups({"question_list", "question_detail"})
     * @Gedmo\SortableGroup
     */
    protected $enabled = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": 0})
     * @JMS\SerializedName("has_comments")
     * @JMS\Groups({"question_detail"})
     */
    protected $hasComments = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": 0})
     * @JMS\SerializedName("has_images")
     * @JMS\Groups({"question_detail"})
     */
    protected $hasImages = false;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Question
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Question
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Question
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @throws \Exception Método "getType" en Entidad "Question" no implementado
     *
     * @return string
     */
    public function getType()
    {
        return new \Exception('Método "getType" en Entidad "Question" no implementado');
    }

    /**
     * @throws \Exception Método "getDiscriminatorColumn" en Entidad "Question" no implementado
     *
     * @return string
     */
    public function getDiscriminatorColumn()
    {
        return new \Exception('Método "getDiscriminatorColumn" en Entidad "Question" no implementado');
    }

    /**
     * Set mandatory.
     *
     * @param bool $mandatory
     *
     * @return Question
     */
    public function setMandatory($mandatory)
    {
        $this->mandatory = $mandatory;

        return $this;
    }

    /**
     * Get mandatory.
     *
     * @return bool
     */
    public function getMandatory()
    {
        return $this->mandatory;
    }

    /**
     * Sets createdAt.
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets updatedAt.
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get page ID.
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("page_id")
     * @JMS\Groups({"question_detail"})
     *
     * @return int
     */
    public function getPageId()
    {
        return null !== $this->page ? $this->page->getId() : null;
    }

    /**
     * Set page.
     *
     * @return Question
     */
    public function setPage(\DSYSurveyBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        if (null !== $page) {
            $page->addQuestion($this);
        }

        return $this;
    }

    /**
     * Get page.
     *
     * @return \DSYSurveyBundle\Entity\Page|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Question
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * set enabled.
     *
     * @return self
     */
    public function setEnabled(bool $state)
    {
        $this->enabled = $state;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * typesNames.
     *
     * @return array
     */
    public static function typesNames()
    {
        return [
            self::TYPE_BOOLEAN => 'Sí o no',
            self::TYPE_TEXT => 'Texto',
            self::TYPE_MULTIPLE_CHOICE => 'Selección múltiple',
            self::TYPE_SINGLE_CHOICE => 'Selección única',
            self::TYPE_GROUP_CHOICE => 'Grupo de Selección',
            self::TYPE_SINGLE_AUTOCOMPLETE => 'Seleccion única (Autocompletado)',
        ];
    }

    /**
     * getTypeName.
     *
     * @return string
     */
    public function getTypeName()
    {
        $names = self::typesNames();

        return null !== $this->getType() ? $names[$this->getType()] : self::TYPE_MULTIPLE_CHOICE;
    }

    /**
     * Get the value of hasComments.
     *
     * @return bool
     */
    public function hasComments()
    {
        return $this->hasComments;
    }

    /**
     * Set the value of hasComments.
     *
     * @return self
     */
    public function setHasComments(bool $hasComments)
    {
        $this->hasComments = $hasComments;

        return $this;
    }

    /**
     * Get the value of hasImages.
     *
     * @return bool
     */
    public function hasImages()
    {
        return $this->hasImages;
    }

    /**
     * Set the value of hasImages.
     *
     * @return self
     */
    public function setHasImages(bool $hasImages)
    {
        $this->hasImages = $hasImages;

        return $this;
    }
}
