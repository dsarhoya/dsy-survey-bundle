<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MultipleChoiceQuestion.
 *
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\MultipleChoiceQuestionRepository")
 */
class MultipleChoiceQuestion extends Question
{
    /**
     * @var Collection MultipleChoiceQuestionAlternative[]
     *
     * @ORM\OneToMany(targetEntity="MultipleChoiceQuestionAlternative", mappedBy="question", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    protected $alternatives;

    /**
     * @var Collection MultipleChoiceQuestionValues[]
     *
     * @ORM\OneToMany(targetEntity="MultipleChoiceQuestionValue", mappedBy="question", cascade={"persist","remove"})
     * @Assert\Valid()
     */
    protected $values;

    /**
     * @var Collection MultipleChoiceQuestionAnswer[]
     *
     * @ORM\OneToMany(targetEntity="MultipleChoiceQuestionAnswer", mappedBy="question")
     */
    protected $questionAnswers;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("type")
     * @JMS\Groups({"question_detail","question_list"})
     */
    private $type;

    /**
     * minima cantidad de respuestas para el MultipleChoice.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\SerializedName("min")
     * @JMS\Groups({"question_detail"})
     */
    private $min;

    /**
     * maxima cantidad de respuestas para el MultipleChoice.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\SerializedName("max")
     * @JMS\Groups({"question_detail"})
     */
    private $max;

    /**
     * Constructor.
     *
     * @param string $type default 'multiplechoice'
     */
    public function __construct(string $type = Question::TYPE_MULTIPLE_CHOICE)
    {
        $this->type = $type;
        $this->alternatives = new ArrayCollection();
        $this->questionAnswers = new ArrayCollection();
        $this->values = new ArrayCollection();
    }

    /**
     * Add alternative.
     *
     * @return MultipleChoiceQuestion
     */
    public function addAlternative(\DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative $alternative)
    {
        if (!$this->alternatives->contains($alternative)) {
            $this->alternatives[] = $alternative;
            $alternative->setQuestion($this);
        }

        return $this;
    }

    /**
     * Remove alternative.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAlternative(\DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative $alternative)
    {
        return $this->alternatives->removeElement($alternative);
    }

    /**
     * Get alternatives.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlternatives()
    {
        return $this->alternatives;
    }

    /**
     * Get alternatives.
     *
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("alternatives")
     * @JMS\Groups({"r_question_alternative"})
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlternativesEnabled()
    {
        $enabled = $this->alternatives->filter(function (EntityEnabledInterface $entity) {
            return true === $entity->isEnabled();
        })->getValues();

        return new ArrayCollection($enabled);
    }

    /**
     * Add questionAnswer.
     *
     * @return MultipleChoiceQuestion
     */
    public function addQuestionAnswer(\DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer $questionAnswer)
    {
        if (!$this->questionAnswers->contains($questionAnswer)) {
            $this->questionAnswers[] = $questionAnswer;
            $questionAnswer->setQuestion($this);
        }

        return $this;
    }

    /**
     * Remove questionAnswer.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeQuestionAnswer(\DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer $questionAnswer)
    {
        return $this->questionAnswers->removeElement($questionAnswer);
    }

    /**
     * Get questionAnswers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionAnswers()
    {
        return $this->questionAnswers;
    }

    /**
     * Set Type.
     *
     * @param string $type
     *
     * @return MultipleChoiceQuestion
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get Type.
     *
     * @return string
     */
    public function getType()
    {
        return null !== $this->type ? $this->type : Question::TYPE_MULTIPLE_CHOICE;
    }

    /**
     * Add value.
     *
     * @return MultipleChoiceQuestionValue
     */
    public function addValue(\DSYSurveyBundle\Entity\MultipleChoiceQuestionValue $value)
    {
        if (!$this->values->contains($value)) {
            $this->values[] = $value;
            $value->setQuestion($this);
        }

        return $this;
    }

    /**
     * Remove value.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeValue(\DSYSurveyBundle\Entity\MultipleChoiceQuestionValue $value)
    {
        return $this->values->removeElement($value);
    }

    /**
     * Get values.
     *
     * @return Collection multipleChoiceQuestionValues[]
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Get alternatives.
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("values")
     * @JMS\Groups({"r_question_value"})
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValuesEnabled()
    {
        $enabled = $this->values->filter(function (EntityEnabledInterface $entity) {
            return true === $entity->isEnabled();
        })->getValues();

        return new ArrayCollection($enabled);
    }

    /**
     * get Choice Question types.
     *
     * @return array string[]
     */
    public static function getChoiceQuestionTypes()
    {
        return [
            self::TYPE_MULTIPLE_CHOICE,
            self::TYPE_SINGLE_CHOICE,
            self::TYPE_GROUP_CHOICE,
            self::TYPE_SINGLE_AUTOCOMPLETE,
        ];
    }

    /**
     * set min.
     *
     * @param int|null $min
     *
     * @return MultipleChoiceQuestion
     */
    public function setMin($min = null)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * get min.
     *
     * @return int|null
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * set max.
     *
     * @param int|null $max
     *
     * @return MultipleChoiceQuestion
     */
    public function setMax($max = null)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * get min.
     *
     * @return int|null
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("discr")
     * @JMS\Groups({"question_list","question_detail"})
     *
     * @return string
     */
    public function getDiscriminatorColumn()
    {
        return $this->getType();
    }
}
