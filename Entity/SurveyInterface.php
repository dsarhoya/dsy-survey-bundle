<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * @author snake77se at dsarhoya.cl
 */
interface SurveyInterface
{
    const STATE_EDIT = 'edit';
    const STATE_PUBLISHED = 'published';
    const STATE_FINISHED = 'finished';

    /**
     * Get ID Survey.
     *
     * @return int
     */
    public function getId();

    /**
     * Get name Survey.
     *
     * @return string
     */
    public function getName();

    /**
     * Get description Survey.
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set code at Survey.
     *
     * @param string|null $code
     *
     * @return SurveyInterface
     */
    public function setCode($code = null);

    /**
     * Get description Survey.
     *
     * @return string|null
     */
    public function getCode();

    /**
     * Get state Survey.
     *
     * @return string
     */
    public function getState();

    /**
     * Get published at Survey.
     *
     * @return \DateTime
     */
    public function getPublishedAt();

    /**
     * Get disabled at Survey.
     *
     * @return \DateTime
     */
    public function getStartedAt();

    /**
     * Get disabled at Survey.
     *
     * @return \DateTime
     */
    public function getFinishedAt();

    /**
     * Get created at Survey.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Get updated at Survey.
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Get states at Survey.
     *
     * @return array
     */
    public static function getStates();

    /**
     * Get label state at Survey.
     *
     * @return string
     */
    public function getStateLabel();

    /**
     * Get surveyAnswers at Survey.
     *
     * @return Collection
     */
    public function getSurveyAnswers();

    /**
     * Get sections at Survey.
     *
     * @return Collection
     */
    public function getSections();

    /**
     * Get sections at Survey.
     *
     * @return Collection
     */
    public function getEnabledSections();

    /**
     * add section at Survey.
     *
     * @return SurveyInterface
     */
    public function addSection(Section $section);

    /**
     * remove section at Survey.
     *
     * @return bool
     */
    public function removeSection(Section $section);

    /**
     * Set state at Survey.
     *
     * @param string $state
     *
     * @return SurveyInterface
     */
    public function setState($state);

    /**
     * Set name at Survey.
     *
     * @return SurveyInterface
     */
    public function setName($name);

    /**
     * Set publishedAt at Survey.
     *
     * @param \DateTime $publishedAt
     *
     * @return SurveyInterface
     */
    public function setPublishedAt($publishedAt);

    /**
     * Set finishedAt at Survey.
     *
     * @param \DateTime $finishedAt
     *
     * @return SurveyInterface
     */
    public function setFinishedAt($finishedAt);

    /**
     * Set startedAt at Survey.
     *
     * @param \DateTime $startedAt
     *
     * @return SurveyInterface
     */
    public function setStartedAt($startedAt);

    /**
     * Set description at Survey.
     *
     * @param string $description
     *
     * @return SurveyInterface
     */
    public function setDescription($description);
}
