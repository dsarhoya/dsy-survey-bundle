<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MultipleChoiceQuestionAlternative.
 *
 * @ORM\Table(name="srv_multiple_choice_question_alternative")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\MultipleChoiceQuestionAlternativeRepository")
 */
class MultipleChoiceQuestionAlternative implements EntityEnabledInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"alternative_list", "alternative_detail"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255)
     * @JMS\SerializedName("title")
     * @JMS\Groups({"alternative_list", "alternative_detail"})
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("code")
     * @JMS\Groups({"alternative_detail"})
     */
    protected $code;

    /**
     * @var MultipleChoiceQuestion
     *
     * @ORM\ManyToOne(targetEntity="MultipleChoiceQuestion", inversedBy="alternatives", cascade={"persist"})
     * @JMS\SerializedName("question")
     * @JMS\Groups({"r_alternative_question", "r_alternative_multiple_choice"})
     */
    protected $question;

    /**
     * @var Collection MultipleChoiceAlternativeAnswer[]
     *
     * @ORM\OneToMany(targetEntity="MultipleChoiceAlternativeAnswer", mappedBy="alternative")
     * @JMS\SerializedName("alternative_answers")
     * @JMS\Groups({"r_alternative_alternative_answer"})
     */
    protected $alternativeAnswers;

    /*
     * Entity Enabled property
     */
    use \DSYSurveyBundle\Traits\EntityEnabledTrait;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->alternativeAnswers = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return MultipleChoiceQuestionAlternative
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set question.
     *
     * @return MultipleChoiceQuestionAlternative|null
     */
    public function setQuestion(\DSYSurveyBundle\Entity\MultipleChoiceQuestion $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return \DSYSurveyBundle\Entity\MultipleChoiceQuestion|null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Get the value of code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code.
     *
     * @param string|null $code
     *
     * @return self
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Add alternativeAnswer.
     *
     * @return MultipleChoiceQuestion
     */
    public function addAlternativeAnswer(\DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer $alternativeAnswer)
    {
        if (!$this->alternativeAnswers->contains($alternativeAnswer)) {
            $this->alternativeAnswers[] = $alternativeAnswer;
            $alternativeAnswer->setAlternative($this);
        }

        return $this;
    }

    /**
     * Remove alternativeAnswer.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAlternativeAnswer(\DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer $alternativeAnswer)
    {
        return $this->alternativeAnswers->removeElement($alternativeAnswer);
    }

    /**
     * Get alternatives.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlternativeAnswers()
    {
        return $this->alternativeAnswers;
    }
}
