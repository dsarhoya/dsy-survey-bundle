<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use DSYSurveyBundle\Validator\Constraints as DSYSurveyAssert;

/**
 * MultipleChoiceAlternativeAnswer.
 *
 * @ORM\Table(name="srv_multiple_choice_alternative_answer")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\MultipleChoiceAlternativeAnswerRepository")
 * @DSYSurveyAssert\GroupChoiceValueNotNull()
 */
class MultipleChoiceAlternativeAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var MultipleChoiceQuestionAlternative
     *
     * @ORM\ManyToOne(targetEntity="MultipleChoiceQuestionAlternative", inversedBy="alternativeAnswers")
     * @JMS\SerializedName("alternative")
     * @JMS\Groups({"r_alternative_answer_alternative"})
     */
    private $alternative;

    /**
     * @var MultipleChoiceQuestionAnswer
     *
     * @ORM\ManyToOne(targetEntity="MultipleChoiceQuestionAnswer", inversedBy="alternativeAnswers")
     * @JMS\SerializedName("question_answer")
     * @JMS\Groups({"r_alternative_answer_question_answer"})
     */
    private $questionAnswer;

    /**
     * @var MultipleChoiceQuestionValue
     *
     * @ORM\ManyToOne(targetEntity="MultipleChoiceQuestionValue", inversedBy="alternativeAnswers")
     * @ORM\JoinColumn(name="value_id", referencedColumnName="id")
     * @JMS\SerializedName("value")
     * @JMS\Groups({"r_alternative_answer_value"})
     */
    protected $value;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alternative.
     *
     * @param \DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative|null $alternative
     *
     * @return MultipleChoiceAlternativeAnswer
     */
    public function setAlternative(\DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative $alternative = null)
    {
        $this->alternative = $alternative;

        return $this;
    }

    /**
     * Get alternative.
     *
     * @return \DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative|null
     */
    public function getAlternative()
    {
        return $this->alternative;
    }

    /**
     * Set questionAnswer.
     *
     * @param \DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer $answer
     *
     * @return MultipleChoiceAlternativeAnswer
     */
    public function setQuestionAnswer(\DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer $questionAnswer)
    {
        $this->questionAnswer = $questionAnswer;

        return $this;
    }

    /**
     * Get questionAnswer.
     *
     * @return \DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer
     */
    public function getQuestionAnswer()
    {
        return $this->questionAnswer;
    }

    /**
     * Set value.
     *
     * @param \DSYSurveyBundle\Entity\MultipleChoiceQuestionValue $value
     *
     * @return MultipleChoiceAlternativeAnswer
     */
    public function setValue(\DSYSurveyBundle\Entity\MultipleChoiceQuestionValue $value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return \DSYSurveyBundle\Entity\MultipleChoiceQuestionValue
     */
    public function getValue()
    {
        return $this->value;
    }
}
