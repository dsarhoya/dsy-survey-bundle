<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * BooleanQuestion entity.
 *
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\BooleanQuestionRepository")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class BooleanQuestion extends Question
{
    /**
     * @var Collection TextQuestionAnswer[]
     *
     * @ORM\OneToMany(targetEntity="TextQuestionAnswer", mappedBy="question")
     */
    protected $answers;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * Add answers.
     *
     * @return BooleanQuestion
     */
    public function addAnswer(BooleanQuestionAnswer $answers)
    {
        if (!$this->answers->contains($answers)) {
            $this->answers[] = $answers;
            $answers->setQuestion($this);
        }

        return $this;
    }

    /**
     * Remove answers.
     */
    public function removeAnswer(BooleanQuestionAnswer $answers)
    {
        $this->answers->removeElement($answers);
    }

    /**
     * Get answers.
     *
     * @return Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Get getQuestionAnswers.
     *
     * @return Collection
     */
    public function getQuestionAnswers()
    {
        return $this->answers;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("type")
     * @JMS\Groups({"question_list","question_detail"})
     *
     * @return string
     */
    public function getType()
    {
        return Question::TYPE_BOOLEAN;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("discr")
     * @JMS\Groups({"question_list","question_detail"})
     *
     * @return string
     */
    public function getDiscriminatorColumn()
    {
        return $this->getType();
    }
}
