<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Description of QuestionAnswer.
 *
 * @ORM\Table(name="srv_question_answer")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\QuestionAnswerRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "answer" = "QuestionAnswer",
 *     "text" = "TextQuestionAnswer",
 *     "bool" = "BooleanQuestionAnswer",
 *     "multiplechoice" = "MultipleChoiceQuestionAnswer",
 * })
 * @UniqueEntity(
 *     fields={"question", "surveyAnswer"},
 *     errorPath="question",
 *     message="Esta pregunta ya fue respondida."
 * )
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class QuestionAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"question_answer_list", "question_answer_detail"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\SerializedName("comment")
     * @JMS\Groups({"question_answer_detail"})
     */
    protected $comment;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\SerializedName("image_key")
     * @JMS\Groups({"question_answer_detail"})
     */
    protected $imageKey;

    /**
     * @var SurveyAnswerInterface
     *
     * @ORM\ManyToOne(targetEntity="SurveyAnswerInterface", inversedBy="questionAnswers")
     * @ORM\JoinColumn(name="survey_answer_id", referencedColumnName="id")
     * @JMS\SerializedName("survey_answer")
     * @JMS\Groups({"r_question_answer_survey_answer"})
     */
    protected $surveyAnswer;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("created_at")
     * @JMS\Groups({"question_answer_detail"})
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("updated_at")
     * @JMS\Groups({"question_answer_detail"})
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="QuestionAnswerImage", mappedBy="question", cascade={"persist"})
     * @JMS\SerializedName("images")
     * @JMS\Groups({"r_question_answer_image"})
     */
    protected $images;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set surveyAnswer.
     *
     * @param SurveyAnswerInterface $surveyAnswer
     *
     * @return QuestionAnswer
     */
    public function setSurveyAnswer(SurveyAnswerInterface $surveyAnswer = null)
    {
        $this->surveyAnswer = $surveyAnswer;

        if (null !== $surveyAnswer && !$surveyAnswer->getQuestionAnswers()->contains($this)) {
            $surveyAnswer->addQuestionAnswer($this);
        }

        return $this;
    }

    /**
     * Get surveyAnswer.
     *
     * @return SurveyAnswerInterface
     */
    public function getSurveyAnswer()
    {
        return $this->surveyAnswer;
    }

    /**
     * Get surveyAnswer id.
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("survey_answer_id")
     * @JMS\Groups({"question_answer_detail"})
     *
     * @return int|null
     */
    public function getSurveyAnswerId()
    {
        return null !== $this->surveyAnswer ? $this->surveyAnswer->getId() : null;
    }

    /**
     * @return string
     */
    public function getType()
    {
        throw new \Exception('Método "getType" en Entidad "QuestionAnswer" no implementado');
    }

    /**
     * @return Question|null
     */
    public function getQuestion()
    {
        throw new \Exception('Método "getQuestion" en Entidad "QuestionAnswer" no implementado');
    }

    /**
     * @return SurveyAnswerInterface
     */
    public function setQuestion(Question $question = null)
    {
        throw new \Exception('Método "setQuestion" en Entidad "QuestionAnswer" no implementado');
    }

    /**
     * @return bool
     */
    public function isAnswered()
    {
        throw new \Exception('Método "isAnswered" en Entidad "QuestionAnswer" no implementado');
    }

    /**
     * set value.
     *
     * @param mixed $value
     *
     * @return QuestionAnswer
     */
    public function setValue($value)
    {
        throw new \Exception('Método "setValue" en Entidad "QuestionAnswer" no implementado');
    }

    /**
     * get value.
     *
     * @return mixed
     */
    public function getValue()
    {
        throw new \Exception('Método "getValue" en Entidad "QuestionAnswer" no implementado');
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return QuestionAnswer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return QuestionAnswer
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * set comment.
     *
     * @param string|null $comment
     *
     * @return QuestionAnswer
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * set image key.
     *
     * @param string|null $imageKey
     *
     * @return QuestionAnswer
     */
    public function setImageKey($imageKey = null)
    {
        $this->imageKey = $imageKey;

        return $this;
    }

    /**
     * get image key.
     *
     * @return string|null
     */
    public function getImageKey()
    {
        return $this->imageKey;
    }

    /**
     * Add image.
     *
     * @return QuestionAnswer
     */
    public function addImage(\DSYSurveyBundle\Entity\QuestionAnswerImage $image)
    {
        $this->images[] = $image;

        $image->setQuestion($this);

        return $this;
    }

    /**
     * Remove image.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeImage(\DSYSurveyBundle\Entity\QuestionAnswerImage $image)
    {
        return $this->images->removeElement($image);
    }

    /**
     * Get images.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}
