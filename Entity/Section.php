<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Section Entity.
 *
 * @ORM\Table(name="srv_section")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\SectionRepository")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class Section implements EntityEnabledInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"section_list", "section_detail"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @JMS\SerializedName("name")
     * @JMS\Groups({"section_list", "section_detail"})
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\SerializedName("description")
     * @JMS\Groups({"section_detail"})
     */
    protected $description;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("code")
     * @JMS\Groups({"section_detail"})
     */
    protected $code;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     * @JMS\SerializedName("position")
     * @JMS\Groups({"section_detail"})
     */
    protected $position;

    /**
     * @var bool
     *
     * @ORM\Column(name="mandatory", type="boolean", options={"default":true})
     * @JMS\SerializedName("mandatory")
     * @JMS\Groups({"section_detail"})
     */
    protected $mandatory = true;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("video_help")
     * @JMS\Groups({"section_detail"})
     */
    protected $videoHelp;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\SerializedName("text_help")
     * @JMS\Groups({"section_detail"})
     */
    protected $textHelp;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("created_at")
     * @JMS\Groups({"section_detail"})
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("updated_at")
     * @JMS\Groups({"section_detail"})
     */
    protected $updatedAt;

    /**
     * @var SurveyInterface
     *
     * @ORM\ManyToOne(targetEntity="SurveyInterface", inversedBy="sections")
     * @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     * @Gedmo\SortableGroup
     * @JMS\SerializedName("survey")
     * @JMS\Groups({"r_section_survey"})
     */
    protected $survey;

    /**
     * @var Collection Pages[]
     *
     * @ORM\OneToMany(targetEntity="Page", mappedBy="section", cascade={"persist"})
     * @Assert\Valid()
     * @JMS\SerializedName("pages")
     * @JMS\Groups({"r_section_page"})
     */
    protected $pages;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": 1})
     * @JMS\SerializedName("enabled")
     * @Gedmo\SortableGroup
     */
    protected $enabled = true;

    /**
     * set enabled.
     *
     * @return Section
     */
    public function setEnabled(bool $state)
    {
        $this->enabled = $state;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Section
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Section
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Section
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set videoHelp.
     *
     * @param string|null $videoHelp
     *
     * @return Section
     */
    public function setVideoHelp($videoHelp = null)
    {
        $this->videoHelp = $videoHelp;

        return $this;
    }

    /**
     * Get videoHelp.
     *
     * @return string|null
     */
    public function getVideoHelp()
    {
        return $this->videoHelp;
    }

    /**
     * Set textHelp.
     *
     * @param string|null $textHelp
     *
     * @return Section
     */
    public function setTextHelp($textHelp = null)
    {
        $this->textHelp = $textHelp;

        return $this;
    }

    /**
     * Get textHelp.
     *
     * @return string|null
     */
    public function getTextHelp()
    {
        return $this->textHelp;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Section
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Section
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add page.
     *
     * @return Section
     */
    public function addPage(\DSYSurveyBundle\Entity\Page $page)
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setSection($this);
        }

        return $this;
    }

    /**
     * Remove page.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removePage(\DSYSurveyBundle\Entity\Page $page)
    {
        return $this->pages->removeElement($page);
    }

    /**
     * Get pages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Get pages.
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("pages")
     * @JMS\Groups({"r_section_pages"})
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnabledPages()
    {
        $enabled = $this->pages->filter(function (EntityEnabledInterface $entity) {
            return true === $entity->isEnabled();
        })->getValues();

        return new ArrayCollection($enabled);
    }

    /**
     * Set survey.
     *
     * @return Section
     */
    public function setSurvey(SurveyInterface $survey = null)
    {
        $this->survey = $survey;

        return $this;
    }

    /**
     * Get survey.
     *
     * @return SurveyInterface|null
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * Get survey.
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("survey_id")
     * @JMS\Groups({"section_detail"})
     *
     * @return int|null
     */
    public function getSurveyId()
    {
        return null !== $this->survey ? $this->survey->getId() : null;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Section
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the value of mandatory.
     *
     * @return bool
     */
    public function getMandatory()
    {
        return $this->mandatory;
    }

    /**
     * Set the value of mandatory.
     *
     * @param bool $mandatory
     *
     * @return self
     */
    public function setMandatory($mandatory)
    {
        $this->mandatory = $mandatory;

        return $this;
    }
}
