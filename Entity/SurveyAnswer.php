<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of SurveyAnswer.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
abstract class SurveyAnswer implements SurveyAnswerInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"survey_answer_list", "survey_answer_detail"})
     */
    protected $id;

    abstract public function getSurvey();

    abstract public function setSurvey(SurveyInterface $survey = null);

    /**
     * @var Collection QuestionAnswer[]
     *
     * @ORM\OneToMany(targetEntity="DSYSurveyBundle\Entity\QuestionAnswer", mappedBy="surveyAnswer", cascade={"persist"})
     */
    protected $questionAnswers;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("created_at")
     * @JMS\Groups({"survey_answer_detail"})
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("updated_at")
     * @JMS\Groups({"survey_answer_detail"})
     */
    protected $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->questionAnswers = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return SurveyAnswerInterface
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return SurveyAnswerInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add questionAnswer.
     *
     * @param QuestionAnswer $answers
     *
     * @return SurveyAnswerInterface
     */
    public function addQuestionAnswer(QuestionAnswer $questionAnswer)
    {
        if (!$this->questionAnswers->contains($questionAnswer)) {
            $this->questionAnswers[] = $questionAnswer;
            $questionAnswer->setSurveyAnswer($this);
        }

        return $this;
    }

    /**
     * Remove questionAnswer.
     *
     * @param QuestionAnswer $answers
     */
    public function removeQuestionAnswer(QuestionAnswer $questionAnswer)
    {
        return $this->questionAnswers->removeElement($questionAnswer);
    }

    /**
     * Get questionAnswers.
     *
     * @return Collection
     */
    public function getQuestionAnswers()
    {
        return $this->questionAnswers;
    }
}
