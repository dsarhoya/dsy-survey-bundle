<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of Survey.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
abstract class Survey implements \DSYSurveyBundle\Entity\SurveyInterface, \DSYSurveyBundle\Entity\EntityEnabledInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"survey_list", "survey_detail"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @JMS\SerializedName("name")
     * @JMS\Groups({"survey_list", "survey_detail"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @JMS\SerializedName("description")
     * @JMS\Groups({"survey_detail"})
     */
    protected $description;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     * @JMS\SerializedName("code")
     * @JMS\Groups({"survey_detail"})
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @JMS\SerializedName("state")
     * @JMS\Groups({"survey_detail"})
     */
    protected $state = \DSYSurveyBundle\Entity\SurveyInterface::STATE_EDIT;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_at", type="datetime", nullable=true)
     * @JMS\SerializedName("published_at")
     * @JMS\Groups({"survey_detail"})
     */
    protected $publishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\SerializedName("started_at")
     * @JMS\Groups({"survey_detail"})
     */
    protected $startedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     * @JMS\SerializedName("disabled_at")
     * @JMS\Groups({"survey_detail"})
     */
    protected $finishedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("created_at")
     * @JMS\Groups({"survey_detail"})
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("updated_at")
     * @JMS\Groups({"survey_detail"})
     */
    protected $updatedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="DSYSurveyBundle\Entity\Section", mappedBy="survey", cascade={"persist","remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @Assert\Valid()
     */
    protected $sections;

    /*
     * Entity Enabled property
     */
    use \DSYSurveyBundle\Traits\EntityEnabledTrait;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->sections = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state.
     *
     * @param string $state
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set publishedAt.
     *
     * @param \DateTime $publishedAt
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt.
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Get Array States constants with label.
     *
     * @return array
     */
    public static function getStates()
    {
        return [
            \DSYSurveyBundle\Entity\SurveyInterface::STATE_EDIT => 'En Edición',
            \DSYSurveyBundle\Entity\SurveyInterface::STATE_PUBLISHED => 'Publicada',
            \DSYSurveyBundle\Entity\SurveyInterface::STATE_FINISHED => 'Finalizada',
        ];
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("state_label")
     * @JMS\Groups({"survey_detail"})
     *
     * @return string
     */
    public function getStateLabel()
    {
        $states = $this->getStates();

        return $states[$this->getState()];
    }

    /**
     * Set finishedAt.
     *
     * @param \DateTime $finishedAt
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setFinishedAt($finishedAt)
    {
        $this->finishedAt = $finishedAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime
     */
    public function getFinishedAt()
    {
        return $this->finishedAt;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets createdAt.
     *
     * @param DateTime $createdAt
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets updatedAt.
     *
     * @param DateTime $updatedAt
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Sets StartedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Returns startedAt.
     *
     * @return DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * set code at survey.
     *
     * @param string|null $code
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * get code at survey.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add section.
     *
     * @param \DSYSurveyBundle\Entity\Section $section
     *
     * @return \DSYSurveyBundle\Entity\SurveyInterface
     */
    public function addSection(\DSYSurveyBundle\Entity\Section $section)
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setSurvey($this);
        }

        return $this;
    }

    /**
     * Remove section.
     *
     * @param \DSYSurveyBundle\Entity\Section $section
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeSection(\DSYSurveyBundle\Entity\Section $section)
    {
        return $this->sections->removeElement($section);
    }

    /**
     * Get sections.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Get sections enabled.
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("sections")
     * @JMS\Groups({"r_survey_section"})
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnabledSections()
    {
        $enabled = $this->sections->filter(function (\DSYSurveyBundle\Entity\Section $section) {
            return true === $section->isEnabled();
        })->getValues();

        return new ArrayCollection($enabled);
    }
}
