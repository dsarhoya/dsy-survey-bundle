<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use DSYSurveyBundle\Validator\Constraints as DSYSurveyAssert;

/**
 * FlowException Entity.
 *
 * @ORM\Table(name="srv_flow_exception")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\FlowExceptionRepository")
 *
 * @DSYSurveyAssert\OnlyFlowExceptionAlternativeForSingleChoice()
 * @DSYSurveyAssert\NewFlowOrEndException()
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class FlowException
{
    const TYPE_ALTERNATIVE = 'alternative';
    const TYPE_QUESTION = 'question';
    const TYPE_FINISH = 'finish';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DSYSurveyBundle\Entity\Question
     *
     * @ORM\ManyToOne(targetEntity="DSYSurveyBundle\Entity\Question")
     * @Assert\NotNull()
     */
    protected $question;

    /**
     * @var \DSYSurveyBundle\Entity\Question
     *
     * @ORM\ManyToOne(targetEntity="DSYSurveyBundle\Entity\Question")
     */
    protected $newFlow;

    /**
     * @var \DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative
     *
     * @ORM\OneToOne(targetEntity="DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative")
     * @ORM\JoinColumn(name="alternative_id", referencedColumnName="id")
     */
    protected $alternative;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * Constructor.
     *
     * @param \DSYSurveyBundle\Entity\Question $question
     */
    public function __construct(\DSYSurveyBundle\Entity\Question $question)
    {
        $this->question = $question;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alternative.
     *
     * @param \DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative|null $alternative
     *
     * @return FlowException
     */
    public function setAlternative(\DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative $alternative = null)
    {
        $this->alternative = $alternative;

        return $this;
    }

    /**
     * Get alternative.
     *
     * @return \DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative|null
     */
    public function getAlternative()
    {
        return $this->alternative;
    }

    /**
     * Set question.
     *
     * @return FlowException|null
     */
    public function setQuestion(\DSYSurveyBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return \DSYSurveyBundle\Entity\Question|null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set NewFlow.
     *
     * @return FlowException|null
     */
    public function setNewFlow(\DSYSurveyBundle\Entity\Question $newFlow = null)
    {
        $this->newFlow = $newFlow;

        return $this;
    }

    /**
     * Get NewFlow.
     *
     * @return \DSYSurveyBundle\Entity\Question|null
     */
    public function getNewFlow()
    {
        return $this->newFlow;
    }

    /**
     * Sets createdAt.
     *
     * @return FlowException
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets updatedAt.
     *
     * @return FlowException
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * get type exception.
     *
     * @return string
     */
    public function getType()
    {
        if (null === $this->newFlow) {
            return self::TYPE_FINISH;
        }
        if (null !== $this->newFlow && null === $this->alternative) {
            return self::TYPE_QUESTION;
        }

        return self::TYPE_ALTERNATIVE;
    }

    /**
     * get types.
     *
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_ALTERNATIVE => 'Alternativa',
            self::TYPE_FINISH => 'Fin',
            self::TYPE_QUESTION => 'Siguiente',
        ];
    }

    /**
     * get typeLabel.
     *
     * @return string
     */
    public function getTypeLabel()
    {
        $temp = self::getTypes();

        return $temp[$this->getType()];
    }
}
