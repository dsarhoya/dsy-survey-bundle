<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * MultipleChoiceQuestionValue.
 *
 * @ORM\Table(name="srv_multiple_choice_question_values")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\MultipleChoiceQuestionValueRepository")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class MultipleChoiceQuestionValue implements EntityEnabledInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"alternative_list", "alternative_detail"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @JMS\SerializedName("title")
     * @JMS\Groups({"alternative_list", "alternative_detail"})
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     * @JMS\SerializedName("position")
     * @JMS\Groups({"question_detail"})
     */
    protected $position;

    /**
     * @var MultipleChoiceQuestion
     *
     * @ORM\ManyToOne(targetEntity="MultipleChoiceQuestion", inversedBy="values")
     * @JMS\SerializedName("question")
     * @JMS\Groups({"r_values_question"})
     * @Assert\NotNull()
     */
    protected $question;

    /**
     * @var Collection MultipleChoiceAlternativeAnswer[]
     *
     * @ORM\OneToMany(targetEntity="MultipleChoiceAlternativeAnswer", mappedBy="value")
     * @JMS\SerializedName("answers")
     * @JMS\Groups({"r_values_question"})
     */
    protected $alternativeAnswers;

    /*
     * Entity Enabled property
     */
    use \DSYSurveyBundle\Traits\EntityEnabledTrait;

    /**
     * Contructor.
     */
    public function __construct()
    {
        $this->alternativeAnswers = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return MultipleChoiceQuestionValue
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return MultipleChoiceQuestionValue
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set question.
     *
     * @return MultipleChoiceQuestionValue
     */
    public function setQuestion(\DSYSurveyBundle\Entity\MultipleChoiceQuestion $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return \DSYSurveyBundle\Entity\MultipleChoiceQuestion
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Add alternativeAnswer.
     *
     * @return MultipleChoiceQuestionValue
     */
    public function addAlternativeAnswer(\DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer $alternativeAnswer)
    {
        if (!$this->alternativeAnswers->contains($alternativeAnswer)) {
            $this->alternativeAnswers[] = $alternativeAnswer;
            $alternativeAnswer->setValue($this);
        }

        return $this;
    }

    /**
     * Remove alternativeAnswer.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAlternativeAnswer(\DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer $alternativeAnswer)
    {
        return $this->alternativeAnswers->removeElement($alternativeAnswer);
    }

    /**
     * Get alternativeAnswers.
     *
     * @return \Doctrine\Common\Collections\Collection MultipleChoiceAlternativeAnswer[]
     */
    public function getAlternativeAnswers()
    {
        return $this->alternativeAnswers;
    }
}
