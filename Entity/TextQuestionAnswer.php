<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Description of TextQuestionAnswer.
 *
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\TextQuestionAnswerRepository")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class TextQuestionAnswer extends QuestionAnswer
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="text_value", type="text", nullable=true)
     * @JMS\SerializedName("value")
     * @JMS\Groups({"question_answer_detail"})
     */
    protected $textValue;

    /**
     * @var TextQuestion
     *
     * @ORM\ManyToOne(targetEntity="TextQuestion", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * @JMS\SerializedName("question")
     * @JMS\Groups({"r_question_answer_question"})
     */
    protected $question;

    /**
     * Set value.
     *
     * @param string $textValue
     *
     * @return TextAnswer
     */
    public function setValue($value)
    {
        $this->textValue = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->textValue;
    }

    /**
     * Set question.
     *
     * @return TextAnswer
     */
    public function setQuestion(Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return Question|null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * {@inheritdoc}
     */
    public function isAnswered()
    {
        return !empty($this->textValue);
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("type")
     * @JMS\Groups({"question_answer_list","question_answer_detail"})
     *
     * @return string
     */
    public function getType()
    {
        return Question::TYPE_TEXT;
    }

    /**
     * Get Question ID.
     *
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("question_id")
     * @JMS\Groups({"question_answer_detail"})
     *
     * @return int
     */
    public function getQuestionId()
    {
        return null !== $this->getQuestion() ? $this->getQuestion()->getId() : null;
    }
}
