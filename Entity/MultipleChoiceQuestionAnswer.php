<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DSYSurveyBundle\Validator\Constraints as DSYSurveyAssert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MultipleChoiceQuestionAnswer.
 *
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\MultipleChoiceQuestionAnswerRepository")
 * @DSYSurveyAssert\SingleChoice
 * @DSYSurveyAssert\GroupChoice
 */
class MultipleChoiceQuestionAnswer extends QuestionAnswer
{
    /**
     * @var MultipleChoiceQuestion
     *
     * @ORM\ManyToOne(targetEntity="MultipleChoiceQuestion", inversedBy="questionAnswers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * @Assert\NotNull()
     * @JMS\SerializedName("question")
     * @JMS\Groups({"r_question_answer_question", "r_multiple_choice_question_answer_multiple_choice_question"})
     */
    protected $question;

    /**
     * @var Collection MultipleChoiceAlternativeAnswer[]
     *
     * @ORM\OneToMany(targetEntity="MultipleChoiceAlternativeAnswer", mappedBy="questionAnswer", cascade={"persist"})
     * @JMS\SerializedName("alternative_answers")
     * @JMS\Groups({"r_question_answer_alternative_answer", "r_multiple_choice_question_answer_multiple_choice_alternative_answer"})
     * @assert\Valid()
     */
    private $alternativeAnswers;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->alternativeAnswers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set question.
     *
     * @return MultipleChoiceQuestion
     */
    public function setQuestion(Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return Question|null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * {@inheritdoc}
     */
    public function isAnswered()
    {
        return $this->alternativeAnswers->count() > 0;
    }

    /**
     * Add alternative answer.
     *
     * @return MultipleChoiceQuestionAnswer
     */
    public function addAlternativeAnswer(\DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer $alternativeAnswer)
    {
        if (!$this->alternativeAnswers->contains($alternativeAnswer)) {
            $this->alternativeAnswers[] = $alternativeAnswer;
            $alternativeAnswer->setQuestionAnswer($this);
        }

        return $this;
    }

    /**
     * Remove alternative.
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeAlternativeAnswer(\DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer $alternativeAnswer)
    {
        return $this->alternativeAnswers->removeElement($alternativeAnswer);
    }

    /**
     * Get alternative answers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlternativeAnswers()
    {
        return $this->alternativeAnswers;
    }

    /**
     * get Alternatives.
     *
     * Este metodo es solo para que el formulario "MultipleChoiceQuestionAnswerType" pueda
     * dibujar las alternativas y apartir de allí con el transformador agregar las
     * respectivas AlternativeAnswer.
     *
     * @return Collection MultipleChoiceAlternativeAnswer[]
     */
    public function getAlternatives()
    {
        return $this->getAlternativeAnswers();
    }

    /**
     * set Alternatives.
     *
     * Este metodo es solo para que el formulario "MultipleChoiceQuestionAnswerType" pueda
     * dibujar las alternativas y apartir de allí con el transformador agregar las
     * respectivas AlternativeAnswer.
     *
     * @return MultipleChoiceQuestionAnswer
     */
    public function setAlternatives(array $alternativeAnswers = [])
    {
        $originalAlternativeAnswers = new ArrayCollection();
        $this->getAlternativeAnswers()->map(function (MultipleChoiceAlternativeAnswer $aa) use ($originalAlternativeAnswers) {
            $originalAlternativeAnswers->add($aa);
        });

        //Agregnado
        /** @var MultipleChoiceAlternativeAnswer $aa */
        foreach ($alternativeAnswers as $aa) {
            if (!$this->alternativeAnswers->contains($aa)) {
                $this->addAlternativeAnswer($aa);
            }
        }

        //Eliminando
        /** @var MultipleChoiceAlternativeAnswer $oAa */
        foreach ($originalAlternativeAnswers as $oAa) {
            if (!in_array($oAa, $alternativeAnswers, true)) {
                $this->removeAlternativeAnswer($oAa);
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return Question::TYPE_MULTIPLE_CHOICE;
    }


    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("question_id")
     * @JMS\Groups({"question_answer_detail"}).
     *
     * @return int|null
     */
    public function getQuestionId()
    {
        return null !== $this->question ? $this->question->getId() : null;
    }
}
