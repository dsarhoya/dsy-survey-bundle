<?php

namespace DSYSurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * QuetionAnswerImage
 *
 * @ORM\Table(name="srv_quetion_answer_image")
 * @ORM\Entity(repositoryClass="DSYSurveyBundle\Repository\QuestionAnswerImageRepository")
 */
class QuestionAnswerImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     * @JMS\SerializedName("image_key")
     * @JMS\Groups({"question_answer_image_detail"})
     */
    protected $imageKey;

    /**
     * @ORM\ManyToOne(targetEntity="QuestionAnswer", inversedBy="images")
     * @JMS\SerializedName("question")
     * @JMS\Groups({"r_image_question_answer"})
     */
    protected $question;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question.
     *
     * @param \DSYSurveyBundle\Entity\QuestionAnswer|null $question
     *
     * @return QuestionAnswerImage
     */
    public function setQuestion(\DSYSurveyBundle\Entity\QuestionAnswer $question = null)
    {
        $this->question = $question;

        if (null !== $question && !$question->getImages()->contains($this)) {
            $question->addImage($this);
        }

        return $this;
    }

    /**
     * Get question.
     *
     * @return \DSYSurveyBundle\Entity\QuestionAnswer|null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set imageKey.
     *
     * @param string $imageKey
     *
     * @return QuestionAnswerImage
     */
    public function setImageKey($imageKey)
    {
        $this->imageKey = $imageKey;

        return $this;
    }

    /**
     * Get imageKey.
     *
     * @return string
     */
    public function getImageKey()
    {
        return $this->imageKey;
    }
}
