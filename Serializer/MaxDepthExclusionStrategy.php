<?php

namespace DSYSurveyBundle\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\Exclusion\ExclusionStrategyInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;

/**
 * Description of MaxDepthExclusionStrategy.
 *
 * @author mati
 */
class MaxDepthExclusionStrategy implements ExclusionStrategyInterface
{
    private $depth;

    public function __construct($depth)
    {
        $this->depth = $depth;
    }

    /**
     * Whether the class should be skipped.
     */
    public function shouldSkipClass(ClassMetadata $metadata, Context $context): bool
    {
        return $this->isTooDeep($context);
    }

    /**
     * Whether the property should be skipped.
     */
    public function shouldSkipProperty(PropertyMetadata $property, Context $context): bool
    {
        return $this->isTooDeep($context);
    }

    private function isTooDeep(Context $context)
    {
        return $context->getDepth() > $this->depth;
    }
}
