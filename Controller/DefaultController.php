<?php

namespace DSYSurveyBundle\Controller;

class DefaultController extends AbstractSurveyController
{
    public function indexAction()
    {
        return $this->render('DSYSurveyBundle:Default:index.html.twig');
    }
}
