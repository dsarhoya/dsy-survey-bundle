<?php

namespace DSYSurveyBundle\Controller\Web;

use DSYSurveyBundle\Controller\AbstractSurveyController;
use DSYSurveyBundle\Form\SurveyType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/surveys")
 */
class SurveyController extends AbstractSurveyController
{
    /**
     * @Route("/index", name="dsy_survey_index")
     */
    public function index(Request $request)
    {
        $params = [];
        if (!$request->query->has('all')) {
            $params['enabled'] = true;
        }
        $surveys = $this->getSurveyRepository()->findBy($params);

        return $this->render('@DSYSurvey/Web/survey/index.html.twig', compact('surveys'));
    }

    /**
     * @Route("/edit", name="dsy_survey_edit")
     */
    public function edit(Request $request)
    {
        $classes = $this->getSurveyClasses();
        $survey = new $classes['survey']['class']();

        $options = [
            'cancel_link' => $this->generateUrl('dsy_survey_index'),
        ];

        if (true === $edit = $request->query->has('survey')) {
            $survey = $this->findSurvey($request->query->get('survey'));
            $options['sections_link'] = $this->generateUrl('dsy_survey_section_index', [
                'survey' => $survey->getId(),
            ]);
        }

        $form = $this->createForm(SurveyType::class, $survey, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('buttons')->get('submit')->isClicked()) {
                if (null === $survey->getId()) {
                    $this->getDoctrine()->getManager()->persist($survey);
                }
                $this->getDoctrine()->getManager()->flush();
                $this->flashSuccess('Guardado exitoso');
            }

            return $this->redirectToRoute($edit ? 'dsy_survey_index' : 'dsy_survey_section_index', [
                'survey' => $survey->getId(),
            ]);
        }

        return $this->render('@DSYSurvey/Web/survey/edit.html.twig', [
            'survey' => $survey,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{survey}/preview", name="dsy_survey_preview")
     */
    public function preview($survey)
    {
        $survey = $this->findSurvey($survey);

        return $this->render('@DSYSurvey/Web/survey/preview.html.twig', ['survey' => $survey]);
    }
}
