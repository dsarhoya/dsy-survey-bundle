<?php

namespace DSYSurveyBundle\Controller\Web;

use DSYSurveyBundle\Controller\AbstractSurveyController;
use DSYSurveyBundle\Entity\Page;
use DSYSurveyBundle\Entity\Section;
use DSYSurveyBundle\Form\PageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/surveys/{survey}/sections/{section}")
 */
class SectionPageController extends AbstractSurveyController
{
    /**
     * @Route("/pages", name="dsy_survey_section_page_index")
     */
    public function index(Request $request, $survey, Section $section)
    {
        $survey = $this->findSurvey($survey);

        $params = ['section' => $section];
        if (!$request->query->has('all')) {
            $params['enabled'] = true;
        }
        $pages = $this->getDoctrine()->getManager()->getRepository(Page::class)->findBy($params);

        return $this->render('@DSYSurvey/Web/page/index.html.twig', [
            'survey' => $survey,
            'section' => $section,
            'pages' => $pages,
        ]);
    }

    /**
     * @Route("/pages/edit", name="dsy_survey_section_page_edit")
     */
    public function new(Request $request, $survey, Section $section)
    {
        $survey = $this->findSurvey($survey);

        $page = new Page();
        $page->setSection($section);

        if (true === $edit = $request->query->has('page')) {
            $page = $this->getDoctrine()->getRepository(Page::class)->find($request->query->get('page'));
        }

        $form = $this->createForm(PageType::class, $page, [
            'cancel_link' => $this->generateUrl('dsy_survey_section_page_index', [
                'survey' => $survey->getId(),
                'section' => $section->getId(),
            ]),
            'questions_link' => !$edit ? null : $this->generateUrl('dsy_survey_section_page_question_index', [
                'survey' => $survey->getId(),
                'section' => $section->getId(),
                'page' => $page->getId(),
            ]),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('buttons')->get('submit')->isClicked()) {
                if (null == $page->getId()) {
                    $this->getDoctrine()->getManager()->persist($page);
                }
                $this->getDoctrine()->getManager()->flush();
                $this->flashSuccess('Guardado exitoso');
            }

            return $this->redirectToRoute($edit ? 'dsy_survey_section_page_index' : 'dsy_survey_section_page_question_index', [
                'survey' => $survey->getId(),
                'section' => $section->getId(),
                'page' => $page->getId(),
            ]);
        }

        return $this->render('@DSYSurvey/Web/page/edit.html.twig', [
            'survey' => $survey,
            'section' => $section,
            'page' => $page,
            'form' => $form->createView(),
        ]);
    }
}
