<?php

namespace DSYSurveyBundle\Controller\Web;

use Doctrine\ORM\EntityRepository;
use DSYSurveyBundle\Controller\AbstractSurveyController;
use DSYSurveyBundle\Entity\FlowException;
use DSYSurveyBundle\Entity\Page;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\Section;
use DSYSurveyBundle\Entity\Survey;
use DSYSurveyBundle\Event\DSYSurveyBundleEvents;
use DSYSurveyBundle\Event\GetFlowExceptionBackUrlEvent;
use DSYSurveyBundle\Form\Web\FlowExceptionType;
use DSYSurveyBundle\Repository\FlowExceptionRepository;
use DSYSurveyBundle\Repository\QuestionRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * FlowException Controller.
 *
 * @Route("/questions/{question}/flow-exceptions")
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class FlowExceptionController extends AbstractSurveyController
{
    /**
     * index.
     *
     * @Route("/", name="dsy_survey_section_page_question_flow_exception_index", methods={"GET","POST"})
     * @Route("/{exception}/edit", name="dsy_survey_section_page_question_flow_exception_edit", methods={"GET", "POST"}, defaults={"exception"=null})
     *
     * @param int|null $exception
     *
     * @return Response
     */
    public function indexAction(Request $request, Question $question, $exception = null)
    {
        $page = $question->getPage();
        $section = $page->getSection();
        $survey = $section->getSurvey();

        if (null !== $exception) {
            /** @var FlowExceptionRepository $exceptionRepo */
            $exceptionRepo = $this->getDoctrine()->getManager()->getRepository(FlowException::class);
            $exception = $exceptionRepo->find($exception);
        }

        $new = false;
        if (null === $exception) {
            $new = true;
            $exception = new FlowException($question);
        }

        /** @var QuestionRepository $questionRepo */
        $questionRepo = $this->getDoctrine()->getManager()->getRepository(Question::class);
        $questions = $questionRepo->questions($survey);

        $options = [
            'questions' => $questions,
            'cancel_link' => $this->generateUrl('dsy_survey_section_page_question_flow_exception_index', ['question' => $question->getId()]),
        ];

        $form = $this->createForm(FlowExceptionType::class, $exception, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($new) {
                $em->persist($exception);
            }
            $em->flush();
            $this->flashSuccess(sprintf('Excepción %s exitosamente.', $new ? 'creada' : 'actualizada'));

            return $this->redirectToRoute('dsy_survey_section_page_question_flow_exception_index', ['question' => $question->getId()]);
        }

        $event = new GetFlowExceptionBackUrlEvent($exception);
        $this->dispatcher()->dispatch($event, DSYSurveyBundleEvents::GET_FLOW_EXCEPTION_BACK_URL);
        $backlUrl = $event->getBackUrl();

        /** @var EntityRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository(FlowException::class);
        $exceptions = $repo->findByQuestion($question);

        $form = $form->createView();

        return $this->render('@DSYSurvey/Web/flow_exception/index.html.twig', compact('survey', 'section', 'page', 'question', 'exceptions', 'questions', 'form', 'exception', 'backlUrl'));
    }

    /**
     * delete flow exception.
     *
     * @Route("/{exception}/delete", name="dsy_survey_section_page_question_flow_exception_delete", methods={"GET"})
     *
     * @return RedirectResponse
     */
    public function deleteAction(Question $question, FlowException $exception)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($exception);
        $em->flush();

        $this->flashSuccess('Excepción borrada exitosamente.');

        return $this->redirectToRoute('dsy_survey_section_page_question_flow_exception_index', ['question' => $question->getId()]);
    }
}
