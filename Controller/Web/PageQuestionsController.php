<?php

namespace DSYSurveyBundle\Controller\Web;

use DSYSurveyBundle\Controller\AbstractSurveyController;
use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\Page;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\Section;
use DSYSurveyBundle\Error\EntityNotFound;
use DSYSurveyBundle\Event\DSYSurveyBundleEvents;
use DSYSurveyBundle\Event\GetQuestionEditUrlsEvent;
use DSYSurveyBundle\Helper\QuestionHelper;
use DSYSurveyBundle\Repository\QuestionRepository;
use DSYSurveyBundle\Services\QuestionEditService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/surveys/{survey}/sections/{section}/pages/{page}")
 */
class PageQuestionsController extends AbstractSurveyController
{
    /**
     * @return QuestionEditService
     */
    public function getQuestionEditService()
    {
        return $this->container->get('dsy.survey_bundle.question_edit.service');
    }

    /**
     * @Route("/questions", name="dsy_survey_section_page_question_index")
     */
    public function index(Request $request, $survey, Section $section, Page $page)
    {
        $survey = $this->getSurveyAndValidateAccess($survey, $section, $page);

        $params = ['page' => $page];
        if (!$request->query->has('all')) {
            $params['enabled'] = true;
        }
        $questions = $this->getDoctrine()->getManager()->getRepository(Question::class)->findBy($params, [
            'enabled' => 'DESC',
            'position' => 'ASC',
        ]);

        return $this->render('@DSYSurvey/Web/question/index.html.twig', [
            'survey' => $survey,
            'section' => $section,
            'page' => $page,
            'questions' => $questions,
            'choicesQuestionTypes' => MultipleChoiceQuestion::getChoiceQuestionTypes(),
        ]);
    }

    /**
     * @Route("/questions/edit", name="dsy_survey_section_page_question_edit")
     */
    public function edit(Request $request, $survey, Section $section, Page $page)
    {
        $survey = $this->getSurveyAndValidateAccess($survey, $section, $page);

        /** @var QuestionRepository $repoQuestion */
        $repoQuestion = $this->getDoctrine()->getRepository(Question::class);

        $question = null;
        if ($request->query->has('question')) {
            $question = $repoQuestion->find($request->query->get('question'));
        } else {
            $question = QuestionHelper::entityFactoryByQuestion($request->query->get('type'));
            $question->setPage($page);
        }

        if ($request->query->has('question') && null === $question) {
            throw new EntityNotFound(Question::class, $request->query->get('question'));
        }

        list($originalAlternatives, $originalValues) = $this->getQuestionEditService()->prepareQuestion($question);

        $event = new GetQuestionEditUrlsEvent($question);
        $this->dispatcher()->dispatch(DSYSurveyBundleEvents::GET_QUESTION_EDIT_URLS, $event);

        $cancelUrl = null !== $event->getCancelUrl()
            ? $event->getCancelUrl()
            : $this->generateUrl('dsy_survey_section_page_question_index', [
                'survey' => $survey->getId(),
                'section' => $section->getId(),
                'page' => $page->getId(),
            ]);

        $form = $this->createForm(QuestionHelper::getFormByQuestion($question->getType()), $question, [
            'cancel_link' => $cancelUrl,
        ]);

        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($form->get('buttons')->get('submit')->isClicked()) {
                $this->getQuestionEditService()->updateQuestion($question, $originalAlternatives, $originalValues);

                if (true === $form->has('alternativesFile') && null !== $form->get('alternativesFile')->getData()) {
                    $this->getQuestionEditService()->updateAlternativesWithFile($question, $form->get('alternativesFile')->getData());
                }

                $em->flush();
                $this->flashSuccess('Guardado exitoso');
            }

            return null !== $event->getSaveUrl()
                ? $event->getSaveUrl()
                : $this->redirectToRoute('dsy_survey_section_page_question_index', [
                    'survey' => $survey->getId(),
                    'section' => $section->getId(),
                    'page' => $page->getId(),
                ]);
        }

        return $this->render('@DSYSurvey/Web/question/edit.html.twig', [
            'survey' => $survey,
            'section' => $section,
            'page' => $page,
            'question' => $question,
            'form' => $form->createView(),
        ]);
    }
}
