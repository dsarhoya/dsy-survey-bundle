<?php

namespace DSYSurveyBundle\Controller\Web;

use DSYSurveyBundle\Controller\AbstractSurveyController;
use DSYSurveyBundle\Entity\Section;
use DSYSurveyBundle\Form\SectionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/surveys/{survey}")
 */
class SurveySectionController extends AbstractSurveyController
{
    /**
     * @Route("/sections", name="dsy_survey_section_index")
     */
    public function index(Request $request, $survey)
    {
        $survey = $this->findSurvey($survey);

        $params = ['survey' => $survey];
        if (!$request->query->has('all')) {
            $params['enabled'] = true;
        }
        $sections = $this->getDoctrine()->getManager()->getRepository(Section::class)->findBy($params);

        return $this->render('@DSYSurvey/Web/section/index.html.twig', [
            'survey' => $survey,
            'sections' => $sections,
        ]);
    }

    /**
     * @Route("/sections/edit", name="dsy_survey_section_edit")
     */
    public function edit(Request $request, string $survey)
    {
        $survey = $this->findSurvey($survey);

        $section = new Section();
        $section->setSurvey($survey);

        if (true === $edit = $request->query->has('section')) {
            $section = $this->getDoctrine()->getRepository(Section::class)->find($request->query->get('section'));
        }

        $form = $this->createForm(SectionType::class, $section, [
            'cancel_link' => $this->generateUrl('dsy_survey_section_index', ['survey' => $survey->getId()]),
            'pages_link' => $edit ? $this->generateUrl('dsy_survey_section_page_index', ['survey' => $survey->getId(), 'section' => $section->getId()]) : null,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('buttons')->get('submit')->isClicked()) {
                if (null === $section->getId()) {
                    $this->getDoctrine()->getManager()->persist($section);
                }
                $this->getDoctrine()->getManager()->flush();
                $this->flashSuccess('Guardado exitoso');
            }

            return $this->redirectToRoute($edit ? 'dsy_survey_section_index' : 'dsy_survey_section_page_index', [
                'survey' => $survey->getId(),
                'section' => $section->getId(),
            ]);
        }

        return $this->render('@DSYSurvey/Web/section/edit.html.twig', [
            'survey' => $survey,
            'section' => $section,
            'form' => $form->createView(),
        ]);
    }
}
