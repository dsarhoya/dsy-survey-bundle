<?php

namespace DSYSurveyBundle\Controller\API;

use DSYSurveyBundle\Controller\AbstractSurveyController;
use DSYSurveyBundle\Event\DSYSurveyBundleEvents;
use DSYSurveyBundle\Event\PostSurveyAnswerIsGrantedEvent;
use DSYSurveyBundle\Services\SurveyAnswerService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * SurveryAnswer API Controller.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class SurveyAnswerController extends AbstractSurveyController
{
    /**
     * Get Surveys.
     *
     * @return Response
     */
    public function getSurveyanswersAction(Request $request)
    {
        throw new \Exception('Endpoint no implementado');
    }

    /**
     * Get Survey.
     *
     * @return Response
     */
    public function getSurveyanswerAction(Request $request, int $survey_answer_id)
    {
        throw new \Exception('Endpoint no implementado');
    }

    /**
     * post surveyAnswer.
     */
    public function postSurveyanswersAction(Request $request)
    {
        $event = new PostSurveyAnswerIsGrantedEvent($request);
        $this->dispatcher()->dispatch($event, DSYSurveyBundleEvents::POST_SURVER_ANSWER_IS_GRANTED);

        /** @var SurveyAnswerService $surveyAnswerSrv */
        $surveyAnswerSrv = $this->container->get('dsy.survey_bundle.survey_answer');
        $surveyAnswer = $surveyAnswerSrv->createSurveyAnswer($request);

        return $this->serializedResponse($surveyAnswer, ['survey_answer_detail']);
    }
}
