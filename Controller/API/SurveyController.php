<?php

namespace DSYSurveyBundle\Controller\API;

use DSYSurveyBundle\Controller\AbstractSurveyController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Survery API Controller.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class SurveyController extends AbstractSurveyController
{
    /**
     * Get Surveys.
     *
     * @return Response
     */
    public function getSurveysAction(Request $request)
    {
        // $this->denyAccessUnlessGranted(self::ACCESS_ATRIBUTE_GET);
        $criteria = [];
        if ($request->query->has('state')) {
            $criteria = array_merge($criteria, ['state' => $request->query->get('state')]);
        }

        $surveys = $this->getSurveyRepository()->findBy($criteria, [
            'updatedAt' => 'desc',
        ]);

        return $this->serializedResponse($surveys, ['survey_list', 'Default']);
    }

    /**
     * Get Survey.
     *
     * @return Response
     */
    public function getSurveyAction(int $survey_id)
    {
        // $this->denyAccessUnlessGranted(self::ACCESS_ATRIBUTE_GET);

        $survey = $this->getSurveyRepository()->find($survey_id);

        return $this->serializedResponse($survey, ['survey_detail', 'Default']);
    }
}
