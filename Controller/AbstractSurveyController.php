<?php

namespace DSYSurveyBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use DSYSurveyBundle\Entity\Page;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\Section;
use DSYSurveyBundle\Entity\SurveyInterface;
use DSYSurveyBundle\Serializer\MaxDepthExclusionStrategy;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of AbstractSurveyController.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
abstract class AbstractSurveyController extends AbstractController
{
    const FLASH_TYPE_INFO = 'info';
    const FLASH_TYPE_SUCCESS = 'success';
    const FLASH_TYPE_ERROR = 'error';
    const FLASH_TYPE_WARNING = 'warning';

    protected function flash($message, $type = self::FLASH_TYPE_INFO)
    {
        $this->get('session')->getFlashBag()->add($type, $message);
    }

    protected function flashSuccess($message)
    {
        $this->flash($message, self::FLASH_TYPE_SUCCESS);
    }

    protected function flashInfo($message)
    {
        $this->flash($message, self::FLASH_TYPE_INFO);
    }

    protected function flashWarning($message)
    {
        $this->flash($message, self::FLASH_TYPE_WARNING);
    }

    protected function flashError($message)
    {
        $this->flash($message, self::FLASH_TYPE_ERROR);
    }

    protected function getSurveyClasses(): array
    {
        /** @var ContainerInterface $container */
        $container = $this->container;

        return $container->getParameter('dsy_survey.classes');
    }

    protected function getSurveyRepository()
    {
        $classes = $this->getSurveyClasses();

        return $this->getDoctrine()->getManager()->getRepository($classes['survey']['class']);
    }

    /**
     * @return SurveyInterface|null
     */
    protected function findSurvey($survey)
    {
        return $this->getSurveyRepository()->find($survey);
    }

    protected function dispatcher(): ? EventDispatcherInterface
    {
        if (!$this->container->has('event_dispatcher')) {
            throw new \LogicException('The event dispatcher is not enabled in your application. You need to define the "event_dispatcher" configuration option.');
        }

        return $this->get('event_dispatcher');
    }

    public static function getSubscribedServices()
    {
        $services = parent::getSubscribedServices();

        return array_merge($services, ['dispatcher' => '?'.EventDispatcherInterface::class]);
    }

    /**
     * validateAccess.
     *
     * @param int $surveyId
     */
    protected function getSurveyAndValidateAccess($surveyId, Section $section, Page $page, Question $question = null)
    {
        $survey = $this->findSurvey($surveyId);

        if (null === $survey) {
            throw EntityNotFoundException::fromClassNameAndIdentifier(SurveyInterface::class, [$survey]);
        }

        if (!$survey->getSections()->contains($section)) {
            throw new \Exception('La sección no pertenece esta encuenta.');
        }
        if (!$section->getPages()->contains($page)) {
            throw new \Exception('La página no pertenece esta sección.');
        }
        if (null !== $question && !$page->getQuestions()->contains($question)) {
            throw new \Exception('La pregunta no pertenece esta página.');
        }

        return $survey;
    }

    /**
     * Crea una respuesta serializada en JSON.
     *
     * @param mixed      $data      La entidad a serializar
     * @param array|null $groups    Los grupos que van a ser serializados y a los cuales las
     *                              propiedades de la entidad pertenecen
     * @param int        $stateCode El código de retorno de la operación
     * @param int        $maxDepth  La profundidad al serializar
     *
     * @return Response La respuesta configurada para la serialización
     */
    protected function serializedResponse($data, $groups = [], $stateCode = 200, $maxDepth = 10)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        if (is_array($request->get('expand'))) {
            $groups = array_merge($groups, $request->get('expand'));
        } elseif (is_array($request->get('expand[]'))) {
            $groups = array_merge($groups, $request->get('expand[]'));
        }
        $serializer = $this->container->get('jms_serializer');
        $strategy = new MaxDepthExclusionStrategy($maxDepth);
        if (is_array($groups)) {
            $response = new Response($serializer->serialize($data, 'json', SerializationContext::create()->setGroups($groups)->addExclusionStrategy($strategy)), $stateCode);
        } else {
            $response = new Response($serializer->serialize($data, 'json', SerializationContext::create()->addExclusionStrategy($strategy)), $stateCode);
        }
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param array $array
     *
     * @return Response
     */
    protected function returnJson($array)
    {
        $response = new Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
