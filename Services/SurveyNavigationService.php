<?php

namespace DSYSurveyBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use DSYSurveyBundle\Entity\FlowException;
use DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\QuestionAnswer;
use DSYSurveyBundle\Entity\SurveyInterface;
use DSYSurveyBundle\Repository\FlowExceptionRepository;
use DSYSurveyBundle\Repository\QuestionRepository;

/**
 * SurveyNavigation Service.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class SurveyNavigationService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var FlowExceptionRepository
     */
    protected $flowExceptionRepo;

    /**
     * @var QuestionRepository
     */
    protected $questionRepo;

    /**
     * Constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->flowExceptionRepo = $this->em->getRepository(FlowException::class);
        $this->questionRepo = $this->em->getRepository(Question::class);
    }

    /**
     * get first survey.
     *
     * @return Question|null
     */
    public function getFirstQuestion(SurveyInterface $survey)
    {
        $questions = $this->questionRepo->questions($survey);

        return count($questions) ? $questions[0] : null;
    }

    /**
     * get next question.
     *
     * @return Question|null
     */
    public function getNextQuestion(Question $question, QuestionAnswer $questionAnswer = null)
    {
        if (in_array($question->getType(), [Question::TYPE_SINGLE_CHOICE/* , Question::TYPE_SINGLE_AUTOCOMPLETE */]) && null !== $questionAnswer) {
            /** @var MultipleChoiceQuestionAnswer $questionAnswer */
            $aa = $questionAnswer->getAlternativeAnswers()->first();

            /** @var MultipleChoiceAlternativeAnswer $aa */
            if (false === $aa) {
                return null;
            }

            return $this->getNextQuestionForAlternative($aa->getAlternative());
        }

        $exception = $this->flowExceptionRepo->findOneByQuestion($question);

        if (null === $exception) {
            return $this->getNextNaturalQuestion($question);
        }

        return $exception->getNewFlow();
    }

    /**
     * get next question for alternative.
     *
     * @return Question|null
     */
    public function getNextQuestionForAlternative(MultipleChoiceQuestionAlternative $alternative)
    {
        $question = $alternative->getQuestion();

        /** @var FlowException $exception */
        $exception = $this->flowExceptionRepo->findOneBy([
            'question' => $question,
            'alternative' => $alternative,
        ]);

        if (null === $exception) {
            return $this->getNextNaturalQuestion($question);
        }

        return $exception->getNewFlow();
    }

    /**
     * get next natural question.
     *
     * @return Question|null
     */
    private function getNextNaturalQuestion(Question $question)
    {
        $survey = $question->getPage()->getSection()->getSurvey();
        $questions = $this->questionRepo->questions($survey);

        $currentIndex = array_search($question, $questions, true);

        return array_key_exists(($currentIndex + 1), $questions) ? $questions[$currentIndex + 1] : null;
    }

    /**
     * has Alternative type FlowExceptions.
     *
     * @return bool
     */
    public function hasAlternativeFlowExceptions(Question $question)
    {
        $exceptions = $this->flowExceptionRepo->findBy([
            'question' => $question,
        ]);

        return array_reduce($exceptions, function ($carry, FlowException $ex) {
            if (FlowException::TYPE_ALTERNATIVE === $ex->getType()) {
                $carry = true;
            }

            return $carry;
        }, false);
    }
}
