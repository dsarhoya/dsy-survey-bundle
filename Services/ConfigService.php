<?php

namespace DSYSurveyBundle\Services;

use DSYSurveyBundle\Entity\Question;

/**
 * @author mati
 */
class ConfigService
{
    private $classes;

    public $baseTemplate;

    public $enableFlowControl;

    /**
     * @var array
     */
    public $includedQuestionTypes;

    public function __construct($classes, $baseTemplate, $includedQuestionTypes, $enableFlowControl)
    {
        $this->classes = [];
        foreach ($classes as $class => $value) {
            $this->classes[$class] = $value['class'];
        }
        $this->baseTemplate = $baseTemplate;

        $this->includedQuestionTypes = $includedQuestionTypes;
        $this->enableFlowControl = $enableFlowControl;

        if (!count($includedQuestionTypes)) {
            $this->includedQuestionTypes = array_keys(Question::typesNames());
        }
    }

    public function getClasses($class = null)
    {
        return null === $class ? $this->classes : $this->classes[$class];
    }
}
