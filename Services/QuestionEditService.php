<?php

namespace DSYSurveyBundle\Services;

use Box\Spout\Common\Entity\Cell;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionValue;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Repository\MultipleChoiceQuestionAlternativeRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * QuestionEditService Service.
 *
 * @author mati
 */
class QuestionEditService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function prepareQuestion(Question $question)
    {
        $originalAlternative = new ArrayCollection();
        $originalValues = new ArrayCollection();

        if (($question instanceof MultipleChoiceQuestion) && (Question::TYPE_SINGLE_AUTOCOMPLETE !== $question->getType())) {
            if (!$question->getAlternatives()->count()) {
                $question->addAlternative(new MultipleChoiceQuestionAlternative());
                $question->addAlternative(new MultipleChoiceQuestionAlternative());
            }

            if (Question::TYPE_GROUP_CHOICE == $question->getType() && !$question->getValues()->count()) {
                $question->addValue(new MultipleChoiceQuestionValue());
                $question->addValue(new MultipleChoiceQuestionValue());
            }

            foreach ($question->getAlternatives() as $alt) {
                $originalAlternative->add($alt);
            }

            foreach ($question->getValues() as $val) {
                $originalValues->add($val);
            }
        }

        return [$originalAlternative, $originalValues];
    }

    public function updateQuestion(Question $question, $originalAlternatives, $originalValues)
    {
        if (!($question instanceof MultipleChoiceQuestion)) {
            return;
        }

        /** @var MultipleChoiceQuestionAlternative $alt */
        foreach ($originalAlternatives as $alt) {
            if (!$question->getAlternatives()->contains($alt)) {
                $question->removeAlternative($alt);
                $this->em->remove($alt);
            }
        }
        if (Question::TYPE_GROUP_CHOICE == $question->getType()) {
            /** @var MultipleChoiceQuestionValue $val */
            foreach ($originalValues as $val) {
                if (!$question->getValues()->contains($val)) {
                    $question->removeValue($val);
                    $this->em->remove($val);
                }
            }
        }
        $this->em->flush();
    }

    public function updateAlternativesWithFile(Question $question, UploadedFile $uploadedFile): self
    {
        if (!($question instanceof MultipleChoiceQuestion) || (Question::TYPE_SINGLE_AUTOCOMPLETE !== $question->getType())) {
            return $this;
        }
        $reader = ReaderEntityFactory::createReaderFromFile($uploadedFile->getClientOriginalName());

        $reader->open($uploadedFile->getRealPath());

        $question->getAlternatives()->map(function (MultipleChoiceQuestionAlternative $alt) {
            $alt->setEnabled(false);
        });
        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $key => $row) {
                if (1 == $key) {
                    continue;
                }
                $this->updateOrCreateAlternative($question, $row->getCells());
            }
        }

        $reader->close();

        return $this;
    }

    public function updateOrCreateAlternative(MultipleChoiceQuestion $question, array $cells): self
    {
        if (2 != count($cells) || !$cells[0] instanceof Cell || $cells[0]->isError() || $cells[0]->isEmpty()) {
            return $this;
        }

        $altCode = (string) $cells[0]->getValue();
        $altTitle = (string) $cells[1]->getValue();

        /** @var MultipleChoiceQuestionAlternativeRepository $alternativeRepo */
        $alternativeRepo = $this->em->getRepository(MultipleChoiceQuestionAlternative::class);

        /** @var MultipleChoiceQuestionAlternative $alternative */
        $alternative = $alternativeRepo->findOneBy([
            'code' => $altCode,
            'question' => $question,
        ]);

        if (null === $alternative) {
            $alternative = new MultipleChoiceQuestionAlternative();
            $alternative->setCode($altCode);
            $question->addAlternative($alternative);
        }

        $alternative->setTitle($altTitle);

        $alternative->setEnabled(true);

        return $this;
    }
}
