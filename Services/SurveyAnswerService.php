<?php

namespace DSYSurveyBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use DSYSurveyBundle\Entity\BooleanQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\QuestionAnswer;
use DSYSurveyBundle\Entity\SurveyAnswerInterface;
use DSYSurveyBundle\Error\InvalidFormError;
use DSYSurveyBundle\Event\DSYSurveyBundleEvents;
use DSYSurveyBundle\Event\PostSubmitQuestionAnswerEvent;
use DSYSurveyBundle\Event\PostSurveyAnswerSaveEvent;
// use DSYSurveyBundle\Event\PostSurveyAnswerSurveyFormEvent;
use DSYSurveyBundle\Form\SurveyAnswerType;
use DSYSurveyBundle\Helper\QuestionAnswerHelper;
use DSYSurveyBundle\Repository\QuestionRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * SurveyAnswer Service.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class SurveyAnswerService
{
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var array
     */
    protected $classes;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * Constructor.
     */
    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $em, array $classes, EventDispatcherInterface $eventDispatcher)
    {
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->classes = $classes;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * createSurveyAnswer.
     *
     * @return SurveyAnswerInterface
     */
    public function createSurveyAnswer(Request $request)
    {
        if (!$request->request->has('answers') || !is_array($request->request->get('answers')) || !count($request->request->get('answers'))) {
            throw new InvalidFormError($this->classes['survey_answer']['class'], "Parameters 'answers' not found.");
        }

        /** @var SurveyAnswerInterface $surveyAnswer */
        $surveyAnswer = new $this->classes['survey_answer']['class']();

        // $event = new PostSurveyAnswerSurveyFormEvent($surveyAnswer);
        // $event = $this->eventDispatcher->dispatch($event, DSYSurveyBundleEvents::POST_SURVER_ANSWER_SURVEY_FORM);

        // if (null === ($form = $event->getForm())) {
        $form = $this->formFactory->create(SurveyAnswerType::class, $surveyAnswer, [
            'data_class' => $this->classes['survey_answer']['class'],
            'survey_class' => $this->classes['survey']['class'],
        ]);
        // }

        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        if (!$form->isValid()) {
            throw new InvalidFormError($this->classes['survey_answer']['class'], (string) $form->getErrors(true, false));
        }

        foreach ($request->request->get('answers') as $answerArray) {
            if (!isset($answerArray['question_id']) || empty($answerArray['question_id'])) {
                throw new InvalidFormError(Question::class, "Parameters 'question_id' not found.");
            }
            /** @var QuestionRepository $repoQuestion */
            $repoQuestion = $this->em->getRepository(Question::class);

            /** @var Question|TextQuestion|BooleanQuestion|MultipleChoiceQuestion|null $question */
            if (null === ($question = $repoQuestion->find($answerArray['question_id']))) {
                throw new InvalidFormError(Question::class, sprintf('Question %s not found.', $answerArray['question_id']));
            }

            $answer = QuestionAnswerHelper::entityFactoryByQuestion($question->getType());
            $answer->setQuestion($question);
            $answer->setSurveyAnswer($surveyAnswer);
            $formClass = QuestionAnswerHelper::getFormByQuestion($question->getType());

            $answerForm = $this->formFactory->create($formClass, $answer, [
                'questions' => $repoQuestion->questions($surveyAnswer->getSurvey()),
            ]);

            /*
             * A veces han llegado llaves en null. Alguna mejor forma de arreglar esto?
             */
            if (isset($answerArray['images'])) {
                $answerArray['images'] = array_filter($answerArray['images'], function ($imageArray) {
                    return isset($imageArray['image_key']) && null !== $imageArray['image_key'];
                });
            }

            $answerForm->submit($answerArray);

            if (!$answerForm->isValid()) {
                throw new InvalidFormError(QuestionAnswer::class, (string) $answerForm->getErrors(true, false));
            }

            $event = new PostSubmitQuestionAnswerEvent($answer, $answerArray);
            $this->eventDispatcher->dispatch($event, DSYSurveyBundleEvents::POST_SUBMIT_QUESTION_ANSWER);
        }

        $this->em->persist($surveyAnswer);
        $this->em->flush();

        $event = new PostSurveyAnswerSaveEvent($surveyAnswer, $data);
        $this->eventDispatcher->dispatch($event, DSYSurveyBundleEvents::POST_SURVER_ANSWER_SAVE);

        return $surveyAnswer;
    }
}
