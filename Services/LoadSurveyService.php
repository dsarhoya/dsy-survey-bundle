<?php

namespace DSYSurveyBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use DSYSurveyBundle\Entity\Page;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\Section;
use DSYSurveyBundle\Entity\SurveyInterface;
use DSYSurveyBundle\Model\Factory\FactoryException;
use DSYSurveyBundle\Model\Factory\QuestionFactory;

class LoadSurveyService
{
    private $em;

    private $configService;

    /**
     * Constructor.
     */
    public function __construct(EntityManagerInterface $em, ConfigService $configService)
    {
        $this->em = $em;
        $this->configService = $configService;
    }

    public function load(array $surveyData)
    {
        /** @var SurveyInterface $survey */
        $survey = $this->em->getRepository(SurveyInterface::class)->findOneBy([
            'name' => $surveyData['name'],
        ]);

        if (null === $survey) {
            $surveyClass = $this->configService->getClasses('survey');
            /** @var SurveyInterface $survey */
            $survey = new $surveyClass();
            FactoryException::throwIfNotSet($surveyData, 'name');
            $survey->setName($surveyData['name']);
            if (isset($surveyData['description'])) {
                $survey->setDescription($surveyData['description']);
            }
            if (isset($surveyData['code'])) {
                $survey->setCode($surveyData['code']);
            }
            $this->em->persist($survey);
            $this->em->flush($survey);
        }

        foreach ($surveyData['sections'] as $sectionData) {
            /** @var Section $section */
            $section = $this->em->getRepository(Section::class)->findOneBy([
                'survey' => $survey,
                'name' => $sectionData['name'],
            ]);

            if (null === $section) {
                $section = new Section();
                FactoryException::throwIfNotSet($sectionData, 'name');
                $section->setName($sectionData['name']);
                $section->setSurvey($survey);
                if (isset($sectionData['code'])) {
                    $section->setCode($sectionData['code']);
                }
            }
            if (isset($sectionData['position'])) {
                $section->setPosition($sectionData['position']);
            }
            if (isset($sectionData['mandatory'])) {
                $section->setMandatory($sectionData['mandatory']);
            }
            if (isset($sectionData['video_help'])) {
                $section->setVideoHelp($sectionData['video_help']);
            }
            if (isset($sectionData['text_help'])) {
                $section->setTextHelp($sectionData['text_help']);
            }
            $this->em->persist($section);
            $this->em->flush($section);

            foreach ($sectionData['pages'] as $pageData) {
                /** @var Page $page */
                $page = $this->em->getRepository(Page::class)->findOneBy([
                    'section' => $section,
                    'title' => $pageData['name'],
                ]);

                if (null === $page) {
                    $page = new Page();
                    FactoryException::throwIfNotSet($pageData, 'name');
                    $page->setTitle($pageData['name']);
                    $page->setSection($section);
                    if (isset($pageData['code'])) {
                        $page->setCode($pageData['code']);
                    }
                }
                if (isset($pageData['html'])) {
                    $page->setHtml($pageData['html']);
                }
                if (isset($pageData['video'])) {
                    $page->setVideo($pageData['video']);
                }
                $this->em->persist($page);
                $this->em->flush($page);

                foreach ($pageData['questions'] as $questionData) {
                    $question = $this->em->getRepository(Question::class)->findOneBy([
                        'page' => $page,
                        'title' => $questionData['title'],
                    ]);

                    if (null === $question) {
                        $question = QuestionFactory::create($questionData);
                        $this->em->persist($question);
                    }

                    $question->setPage($page);
                    if (isset($questionData['position'])) {
                        $question->setPosition($questionData['position']);
                    }
                    if (isset($questionData['description'])) {
                        $question->setDescription($questionData['description']);
                    }
                    if (isset($questionData['has_comments'])) {
                        $question->setHasComments((bool) $questionData['has_comments']);
                    }
                    if (isset($questionData['has_images'])) {
                        $question->setHasImages((bool) $questionData['has_images']);
                    }
                    if (isset($questionData['mandatory'])) {
                        $question->setMandatory((bool) $questionData['mandatory']);
                    }
                    if (isset($questionData['input_type']) && Question::TYPE_TEXT === $question->getType()) {
                        $question->setInputType($questionData['input_type']);
                    }

                    $this->em->flush($question);
                }
            }
        }
    }
}
