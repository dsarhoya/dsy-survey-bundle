<?php

namespace DSYSurveyBundle\Model\Factory;

use DSYSurveyBundle\Entity\BooleanQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionValue;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\TextQuestion;

class QuestionFactory
{
    public static function create(array $data)
    {
        if (!isset($data['type'])) {
            FactoryException::throw('Type needed');
        }

        switch ($data['type']) {
            case Question::TYPE_BOOLEAN:
                return self::boolean($data);
            case Question::TYPE_TEXT:
                return self::text($data);
            case Question::TYPE_SINGLE_CHOICE:
            case Question::TYPE_MULTIPLE_CHOICE:
            case Question::TYPE_GROUP_CHOICE:
            case Question::TYPE_SINGLE_AUTOCOMPLETE:
                return self::multiplechoice($data, $data['type']);
            default:
            FactoryException::throw('Unknown type');
        }
    }

    public static function boolean(array $data)
    {
        $question = new  BooleanQuestion();
        self::setBasicDataQuestion($question, $data);

        return $question;
    }

    public static function text(array $data)
    {
        $question = new  TextQuestion();
        self::setBasicDataQuestion($question, $data);

        return $question;
    }

    public static function multiplechoice(array $data, $type = Question::TYPE_MULTIPLE_CHOICE)
    {
        $question = new  MultipleChoiceQuestion($type);
        self::setBasicDataQuestion($question, $data);
        FactoryException::throwIfNotSet($data, 'alternatives');
        if (Question::TYPE_GROUP_CHOICE == $type) {
            FactoryException::throwIfNotSet($data, 'values');
        }

        //NOTE Alternatives
        foreach ($data['alternatives'] as $alternativeData) {
            FactoryException::throwIfNotSet($alternativeData, 'title');
            $alternative = new MultipleChoiceQuestionAlternative();
            $alternative->setTitle($alternativeData['title']);
            if (isset($alternativeData['code'])) {
                $alternative->setCode($alternativeData['code']);
            }
            $question->addAlternative($alternative);
        }

        // NOTE Values
        if (Question::TYPE_GROUP_CHOICE == $type && is_array($data['values'])) {
            foreach ($data['values'] as $valueData) {
                FactoryException::throwIfNotSet($valueData, 'title');
                $value = new MultipleChoiceQuestionValue();
                $value->setTitle($valueData['title']);
                $question->addValue($value);
            }
        }

        return $question;
    }

    /**
     * set Basic Data Question.
     */
    private static function setBasicDataQuestion(Question $question, array $data)
    {
        FactoryException::throwIfNotSet($data, 'title');
        $question->setTitle($data['title']);
        if (isset($data['code'])) {
            $question->setCode($data['code']);
        }
    }
}
