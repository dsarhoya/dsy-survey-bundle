<?php

namespace DSYSurveyBundle\Model\Factory;

class FactoryException extends \Exception
{
    public static function throw($message)
    {
        throw new self($message);
    }

    public static function throwIfNotSet($data, $index)
    {
        if (!isset($data[$index])) {
            throw new self("$index needed");
        }
    }
}
