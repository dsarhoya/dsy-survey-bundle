<?php

namespace DSYSurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class PostSurveyAnswerIsGrantedEvent extends Event
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the value of request.
     */
    public function getRequest(): ? Request
    {
        return $this->request;
    }
}
