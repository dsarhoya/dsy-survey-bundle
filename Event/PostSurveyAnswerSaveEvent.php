<?php

namespace DSYSurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of PostSurveyAnswerEvent.
 *
 * @author jcordova <jhonny.cordova@dsarhoya.cl>
 */
class PostSurveyAnswerSaveEvent extends Event
{
    protected $surveyAnswerEntity;

    protected $surverAnswerData;

    public function __construct($surveyAnswerEntity, $surverAnswerData)
    {
        $this->surveyAnswerEntity = $surveyAnswerEntity;
        $this->surverAnswerData = $surverAnswerData;
    }

    /**
     * Get the value of surveyAnswerEntity.
     */
    public function getSurveyAnswerEntity()
    {
        return $this->surveyAnswerEntity;
    }

    /**
     * Get the value of surverAnswerData.
     */
    public function getSurverAnswerData()
    {
        return $this->surverAnswerData;
    }
}
