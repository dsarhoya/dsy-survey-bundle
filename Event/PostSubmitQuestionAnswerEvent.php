<?php

namespace DSYSurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of PostSubmitQuestionAnswerEvent.
 *
 * @author jcordova <jhonny.cordova@dsarhoya.cl>
 */
class PostSubmitQuestionAnswerEvent extends Event
{
    protected $questionAnswerEntity;

    protected $questionAnswerData;

    public function __construct($questionAnswerEntity, $questionAnswerData)
    {
        $this->questionAnswerEntity = $questionAnswerEntity;
        $this->questionAnswerData = $questionAnswerData;
    }

    /**
     * Get the value of questionAnswerEntity
     */ 
    public function getQuestionAnswerEntity()
    {
        return $this->questionAnswerEntity;
    }


    /**
     * Get the value of questionAnswerData
     */ 
    public function getQuestionAnswerData()
    {
        return $this->questionAnswerData;
    }

}
