<?php

namespace DSYSurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of GetCustomConstraintQuestionAnswerEvent.
 *
 * @author jcordova <jhonny.cordova@dsarhoya.cl>
 */
class GetCustomConstraintQuestionAnswerEvent extends Event
{
    protected $constraints;

    public function __construct()
    {
        $this->constraints = [];
    }

    /**
     * Get the value of constraints.
     */
    public function getConstraints()
    {
        return $this->constraints;
    }

    /**
     * Set the value of constraints.
     *
     * @return self
     */
    public function setConstraints($constraints)
    {
        $this->constraints = $constraints;

        return $this;
    }
}
