<?php

namespace DSYSurveyBundle\Event;

/**
 * Description of DSYSurveyBundleEvents.
 *
 * @author jcordova <jhonny.cordova@dsarhoya.cl>
 */
final class DSYSurveyBundleEvents
{
    /** @Event("DSYSurveyBundle\Event\GetCustomConstraintQuestionAnswerEvent") */
    const GET_CUSTOM_CONSTRAINTS_QUESTION_ANSWER = 'dsy_survey_bundle.GetCustomConstraintsQuestionAswer';

    /** @Event("DSYSurveyBundle\Event\PostSurveyAnswerSaveEvent") */
    const POST_SURVER_ANSWER_SAVE = 'dsy_survey_bundle.PostSurveyAnswerSaveEvent';

    /** @Event("DSYSurveyBundle\Event\PostSurveyAnswerIsGrantedEvent") */
    const POST_SURVER_ANSWER_IS_GRANTED = 'dsy_survey_bundle.PostSurveyAnswerIsGrantedEvent';

    // /** @Event("DSYSurveyBundle\Event\PostSurveyAnswerSurveyFormEvent") */
    // const POST_SURVER_ANSWER_SURVEY_FORM = 'dsy_survey_bundle.PostSurveyAnswerSurveyFormEvent';

    /** @Event("DSYSurveyBundle\Event\PostSubmitQuestionAnswerEvent") */
    const POST_SUBMIT_QUESTION_ANSWER = 'dsy_survey_bundle.PostSubmitQuestionAnswerEvent';

    /** @Event("DSYSurveyBundle\Event\GetQuestionEditUrlsEvent") */
    const GET_QUESTION_EDIT_URLS = 'dsy_survey_bundle.GetQuestionEditUrlsEvent';

    /** @Event("DSYSurveyBundle\Event\GetFlowExceptionBackUrlEvent") */
    const GET_FLOW_EXCEPTION_BACK_URL = 'dsy_survey_bundle.GetFlowExceptionBackUrlEvent';
}
