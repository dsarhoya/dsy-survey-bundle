<?php

namespace DSYSurveyBundle\Event;

use DSYSurveyBundle\Entity\SurveyAnswerInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\FormInterface;

/**
 * PostSurveyAnswerSurveyForm Event.
 *
 * @author snaje77se <yosip.curiel@dsarhoya.cl>
 */
class PostSurveyAnswerSurveyFormEvent extends Event
{
    protected $surveyAnswerEntity;

    protected $form;

    public function __construct(SurveyAnswerInterface $surveyAnswerEntity)
    {
        $this->surveyAnswerEntity = $surveyAnswerEntity;
    }

    /**
     * Get the value of surveyAnswerEntity.
     */
    public function getSurveyAnswerEntity()
    {
        return $this->surveyAnswerEntity;
    }

    /**
     * Get the value of form.
     */
    public function getForm(): ? FormInterface
    {
        return $this->form;
    }

    /**
     * Set the value of form.
     *
     * @return self
     */
    public function setForm(FormInterface $form)
    {
        $this->form = $form;

        return $this;
    }
}
