<?php

namespace DSYSurveyBundle\Event;

use DSYSurveyBundle\Entity\FlowException;
use Symfony\Component\EventDispatcher\Event;

/**
 * GetFlowExceptionBackUrl Event.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class GetFlowExceptionBackUrlEvent extends Event
{
    /**
     * exception.
     *
     * @var FlowException
     */
    protected $exception;

    /**
     * @var string|null
     */
    protected $backUrl;

    /**
     * Contructor.
     *
     * @param FlowException $exception
     */
    public function __construct(FlowException $exception)
    {
        $this->exception = $exception;
    }

    /**
     * Get exception.
     *
     * @return FlowException
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * Set exception.
     *
     * @param FlowException $exception
     *
     * @return GetFlowExceptionBackUrlEvent
     */
    public function setException(FlowException $exception)
    {
        $this->exception = $exception;

        return $this;
    }

    /**
     * Get the value of backUrl.
     *
     * @return string|null
     */
    public function getBackUrl()
    {
        return $this->backUrl;
    }

    /**
     * Set the value of backUrl.
     *
     * @param string|null $backUrl
     *
     * @return GetFlowExceptionBackUrlEvent
     */
    public function setBackUrl(string $backUrl = null)
    {
        $this->backUrl = $backUrl;

        return $this;
    }
}
