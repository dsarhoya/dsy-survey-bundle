<?php

namespace DSYSurveyBundle\Event;

use DSYSurveyBundle\Entity\Question;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

/**
 * GetQuestionEditUrls Event.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class GetQuestionEditUrlsEvent extends Event
{
    /**
     * question.
     *
     * @var Question
     */
    protected $question;

    /**
     * @var Response|null
     */
    protected $saveUrl;

    /**
     * @var string|null
     */
    protected $cancelUrl;

    /**
     * Contructor.
     */
    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    /**
     * Get question.
     *
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set question.
     *
     * @param Question $question question
     *
     * @return GetQuestionEditUrlsEvent
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get the value of saveUrl.
     *
     * @return Response|null
     */
    public function getSaveUrl()
    {
        return $this->saveUrl;
    }

    /**
     * Set the value of saveUrl.
     *
     * @return GetQuestionEditUrlsEvent
     */
    public function setSaveUrl(Response $saveUrl = null)
    {
        $this->saveUrl = $saveUrl;

        return $this;
    }

    /**
     * Get the value of cancelUrl.
     *
     * @return string|null
     */
    public function getCancelUrl()
    {
        return $this->cancelUrl;
    }

    /**
     * Set the value of cancelUrl.
     *
     * @return GetQuestionEditUrlsEvent
     */
    public function setCancelUrl(string $cancelUrl = null)
    {
        $this->cancelUrl = $cancelUrl;

        return $this;
    }
}
