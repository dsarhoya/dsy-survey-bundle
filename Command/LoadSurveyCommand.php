<?php

namespace DSYSurveyBundle\Command;

use DSYSurveyBundle\Services\LoadSurveyService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpKernel\KernelInterface;

class LoadSurveyCommand extends Command
{
    /**
     * @var LoadSurveyService
     */
    private $loadSurveySrv;

    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * Constructor.
     */
    public function __construct(LoadSurveyService $loadSurveySrv, KernelInterface $kernel)
    {
        $this->loadSurveySrv = $loadSurveySrv;
        $this->kernel = $kernel;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('dsy:survey:load-survey')
            ->addArgument('jsonFile', InputArgument::REQUIRED, 'Route to json file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $jsonData = file_get_contents($this->kernel->getProjectDir().DIRECTORY_SEPARATOR.$input->getArgument('jsonFile'));
        $surveyData = json_decode($jsonData, true);

        $this->loadSurveySrv->load($surveyData);

        (new SymfonyStyle($input, $output))->success('Listo');
    }
}
