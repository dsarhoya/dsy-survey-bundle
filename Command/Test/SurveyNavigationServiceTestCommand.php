<?php

namespace DSYSurveyBundle\Command\Test;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAlternative;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\SurveyInterface;
use DSYSurveyBundle\Repository\QuestionRepository;
use DSYSurveyBundle\Services\SurveyNavigationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * SurveyNavigationService Test.
 */
class SurveyNavigationServiceTestCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var SurveyNavigationService
     */
    protected $surveyNavSrv;

    public function __construct(EntityManagerInterface $em, SurveyNavigationService $surveyNavSrv)
    {
        parent::__construct();
        $this->em = $em;
        $this->surveyNavSrv = $surveyNavSrv;
    }

    protected function configure()
    {
        $this->setName('dsy:survey-test:survey-nav');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityRepository $surveyRepo */
        $surveyRepo = $this->em->getRepository(SurveyInterface::class);
        $survey = $surveyRepo->findOneByName('Example survey 2');

        /** @var QuestionRepository $questionRepo */
        $questionRepo = $this->em->getRepository(Question::class);

        $questions = $questionRepo->questions($survey);

        // TEST First question
        // $firstQuestion = $this->surveyNavSrv->getFirstQuestion($survey);
        // dump($firstQuestion->getCode());

        /** @var Question $q */
        foreach ($questions as $q) {
            $nextQuestion = null;
            $output->writeln('---------------------------');
            // TEST Ordenamiento
            // dump($q->getCode());

            // TEST Get Next Natural Question
            if (Question::TYPE_SINGLE_CHOICE !== $q->getType()) {
                $nextQuestion = $this->surveyNavSrv->getNextQuestion($q);
                $output->writeln($q->getCode()." {$q->getType()}".' => '.(null !== $nextQuestion ? $nextQuestion->getCode()." {$nextQuestion->getType()}" : 'NULL'));
            } else {
                /** @var MultipleChoiceQuestionAlternative $alt */
                /** @var MultipleChoiceQuestion $q */
                foreach ($q->getAlternatives() as $alt) {
                    $nextQuestion = $this->surveyNavSrv->getNextQuestionForAlternative($alt);
                    $output->writeln($q->getCode()." {$q->getType()}".' Alt: '.$alt->getTitle().' => '.(null !== $nextQuestion ? $nextQuestion->getCode()." {$nextQuestion->getType()}" : 'FIN'));
                }
            }
        }
        $output->writeln('---------------------------');
        $output->writeln('FIN!');
    }
}
