<?php

namespace DSYSurveyBundle\Error;

/**
 * BaseApiError.
 */
abstract class BaseApiError extends \Exception
{
    protected $internalCode = null;
    protected $info = null;

    public function __construct($message = 'Ocurrión un error', $code = 500)
    {
        parent::__construct($message, $code);
    }

    public function setInternalCode($internalCode)
    {
        $this->internalCode = $internalCode;

        return $this;
    }

    public function getInternalCode()
    {
        return $this->internalCode;
    }

    public function setInfo(array $info)
    {
        $this->info = $info;

        return $this;
    }

    public function getInfo()
    {
        return $this->info;
    }
}
