<?php

namespace DSYSurveyBundle\Error;

class InvalidDateTime extends Error400
{
    public function __construct($dateTime, $format = 'YYYY-MM-DD')
    {
        parent::__construct("Formato de fecha invalido '{$dateTime}', formato valido '{$format}'.");
    }
}
