<?php

namespace DSYSurveyBundle\Error;

class PersistEntityError extends Error400
{
    public function __construct($entity = null)
    {
        parent::__construct("Hubo un error al persistir la entidad {$entity}");
    }
}
