<?php

namespace DSYSurveyBundle\Error;

class ParameterNotFound extends Error400
{
    public function __construct($parameter)
    {
        parent::__construct("No se encontró el parámetro $parameter");
    }
}
