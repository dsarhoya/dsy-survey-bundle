<?php

namespace DSYSurveyBundle\Error;

class EntityNotFound extends Error400
{
    public function __construct($entity, $entityId)
    {
        parent::__construct(sprintf("No se encontró el entidad '%s', %d", $entity, $entityId));
    }
}
