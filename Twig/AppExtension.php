<?php

namespace DSYSurveyBundle\Twig;

use Doctrine\ORM\EntityManagerInterface;
use DSYSurveyBundle\Entity\Question;
use DSYSurveyBundle\Entity\SurveyInterface;
use DSYSurveyBundle\Repository\QuestionRepository;
use DSYSurveyBundle\Services\ConfigService;
use DSYSurveyBundle\Services\SurveyNavigationService;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Twig Extension.
 */
class AppExtension extends AbstractExtension
{
    /**
     * @var ConfigService
     */
    private $configService;

    /**
     * @var SurveyNavigationService
     */
    private $surveyNavSrv;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Constructor.
     */
    public function __construct(ConfigService $configService, SurveyNavigationService $surveyNavSrv, EntityManagerInterface $em)
    {
        $this->configService = $configService;
        $this->surveyNavSrv = $surveyNavSrv;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('baseTemplate', [$this, 'baseTemplate']),
            new TwigFunction('isFlowControlEnabled', [$this, 'isFlowControlEnabled']),
            new TwigFunction('nextQuestion', [$this->surveyNavSrv, 'getNextQuestion']),
            new TwigFunction('nextQuestionForAlternative', [$this->surveyNavSrv, 'getNextQuestionForAlternative']),
            new TwigFunction('firstQuestion', [$this->surveyNavSrv, 'getFirstQuestion']),
            new TwigFunction('hasAlternativeFlowExceptions', [$this->surveyNavSrv, 'hasAlternativeFlowExceptions']),
            new TwigFunction('showQuestionsFlow', [$this, 'showQuestionsFlow'], [
                'is_safe' => ['html'],
                'needs_environment' => true,
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('isTypeIncluded', [$this, 'isTypeIncluded']),
            new TwigFilter('firstLetterType', [$this, 'getFirstLetterType']),
        ];
    }

    public function baseTemplate()
    {
        return $this->configService->baseTemplate;
    }

    public function isTypeIncluded($type)
    {
        $types = $this->configService->includedQuestionTypes;

        return in_array($type, $types);
    }

    public function isFlowControlEnabled()
    {
        return $this->configService->enableFlowControl;
    }

    /**
     * showQuestionsFlow.
     *
     * @param SurveyInterface $survey
     * @param string|null     $template
     *
     * @return string
     */
    public function showQuestionsFlow(Environment $env, $entity, $template = '@DSYSurvey/Web/flow_exception/_preview_questions_flow.html.twig')
    {
        switch (true) {
            case $entity instanceof SurveyInterface:
                /** @var QuestionRepository $repo */
                $repo = $this->em->getRepository(Question::class);

                $questions = $repo->questions($entity);
                break;
            case $entity instanceof Question:
                $questions = [$entity];
                break;

            default:
                throw new \Exception('Clase no soportada dibujar flujo de preguntas');
        }

        return $env->render($template, compact('questions'));
    }

    public function getFirstLetterType(Question $question)
    {
        if (Question::TYPE_SINGLE_AUTOCOMPLETE == $question->getType()) {
            return 'A';
        }

        $arr = str_split($question->getType());

        return strtoupper($arr[0]);
    }
}
