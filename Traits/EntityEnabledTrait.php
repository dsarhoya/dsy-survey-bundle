<?php

namespace DSYSurveyBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

trait EntityEnabledTrait
{
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": 1})
     * @JMS\SerializedName("enabled")
     */
    protected $enabled = true;

    /**
     * set enabled.
     *
     * @param bool $state
     *
     * @return self
     */
    public function setEnabled(bool $state)
    {
        $this->enabled = $state;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
}
