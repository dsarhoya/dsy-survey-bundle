<?php

namespace DSYSurveyBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class DSYSurveyExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (!isset($config['classes'])) {
            $config['classes']['survey'] = $this->addSurveyConfiguration();
            $config['classes']['survey_answer'] = $this->addSurveyAnswerConfiguration();
        }

        if (!isset($config['classes']['survey'])) {
            $config['classes']['survey'] = $this->addSurveyConfiguration();
        }

        if (!isset($config['classes']['survey_answer'])) {
            $config['classes']['survey_answer'] = $this->addSurveyAnswerConfiguration();
        }

        $container->setParameter('dsy_survey.classes', $config['classes']);
        $container->setParameter('dsy_survey.base_template', $config['base_template']);
        $container->setParameter('dsy_survey.included_question_types', $config['included_question_types']);
        $container->setParameter('dsy_survey.enable_flow_control', $config['enable_flow_control']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    private function addSurveyConfiguration()
    {
        return array('class' => 'DSYSurveyBundle\Entity\Survey');
    }

    private function addSurveyAnswerConfiguration()
    {
        return array('class' => 'DSYSurveyBundle\Entity\SurveyAnswer');
    }
}
