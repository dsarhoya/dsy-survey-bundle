<?php

namespace DSYSurveyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dsy_survey');

        $rootNode
            ->children()
                ->scalarNode('base_template')->defaultValue('base.html.twig')->end()
                ->arrayNode('included_question_types')
                ->scalarPrototype()->end()
                ->end()
                ->scalarNode('enable_flow_control')->defaultValue(true)->end()
                ->arrayNode('classes')
                    ->children()
                        ->arrayNode('survey')
                            ->children()
                                ->scalarNode('class')->end()
                            ->end()
                        ->end()
                        ->arrayNode('survey_answer')
                            ->children()
                                ->scalarNode('class')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
