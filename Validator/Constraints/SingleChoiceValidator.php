<?php

namespace DSYSurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer;
use DSYSurveyBundle\Entity\Question;

/**
 * SingleChoice Validator.
 */
class SingleChoiceValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof SingleChoice) {
            throw new UnexpectedTypeException($constraint, SingleChoice::class);
        }

        if (null === $value || !$value instanceof MultipleChoiceQuestionAnswer || null === $value->getQuestion()) {
            return;
        }

        $question = $value->getQuestion();
        if (Question::TYPE_SINGLE_CHOICE == $question->getType() && count($value->getAlternativeAnswers()) > 1) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ count }}', count($value->getAlternativeAnswers()))
                ->addViolation();
        }
    }
}
