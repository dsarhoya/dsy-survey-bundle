<?php

namespace DSYSurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class GroupChoiceValueNotNull extends Constraint
{
    public $message = 'Debe seleccionar un valor.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
