<?php

namespace DSYSurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class GroupChoice extends Constraint
{
    public $message = 'Debes responder todas las preguntas.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
