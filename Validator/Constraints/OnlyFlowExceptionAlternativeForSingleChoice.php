<?php

namespace DSYSurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class OnlyFlowExceptionAlternativeForSingleChoice extends Constraint
{
    public $message = 'Sólo se permite una excepción de flujo de tipo alterantiva para preguntas de tipo Selección simple.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
