<?php

namespace DSYSurveyBundle\Validator\Constraints;

use DSYSurveyBundle\Entity\FlowException;
use DSYSurveyBundle\Entity\Question;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * OnlyFlowExceptionAlternativeForSingleChoice Validator.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class OnlyFlowExceptionAlternativeForSingleChoiceValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($entity, Constraint $constraint)
    {
        if (!$constraint instanceof OnlyFlowExceptionAlternativeForSingleChoice) {
            throw new UnexpectedTypeException($constraint, OnlyFlowExceptionAlternativeForSingleChoice::class);
        }

        if (null === $entity || !$entity instanceof FlowException) {
            return;
        }

        $question = $entity->getQuestion();
        $availableQuestionType = [Question::TYPE_SINGLE_CHOICE/* , Question::TYPE_SINGLE_AUTOCOMPLETE */];
        if (null !== $entity->getAlternative() && !in_array($question->getType(), $availableQuestionType)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
