<?php

namespace DSYSurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class NewFlowOrEndException extends Constraint
{
    public $message = 'Ya cuenta con una exception de flujo para esta pregunta o alternativa.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
