<?php

namespace DSYSurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class SingleChoice extends Constraint
{
    public $message = 'Sólo se permite una alternativa, venían {{ count }}';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
