<?php

namespace DSYSurveyBundle\Validator\Constraints;

use DSYSurveyBundle\Entity\MultipleChoiceQuestion;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use DSYSurveyBundle\Entity\MultipleChoiceQuestionAnswer;

/**
 * GroupChoice Validator.
 */
class GroupChoiceValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof GroupChoice) {
            throw new UnexpectedTypeException($constraint, GroupChoice::class);
        }
        
        if (!$value instanceof MultipleChoiceQuestionAnswer || null === $value->getQuestion()) {
            return;
        }
        
        /** @var MultipleChoiceQuestionAnswer $value */
        /** @var MultipleChoiceQuestion $question */
        $question = $value->getQuestion();
        
        if ($question->getType() !== MultipleChoiceQuestion::TYPE_GROUP_CHOICE) {
            return;
        }

        if (count($question->getAlternativesEnabled()) !== count($value->getAlternativeAnswers())) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
