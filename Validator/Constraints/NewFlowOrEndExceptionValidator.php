<?php

namespace DSYSurveyBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use DSYSurveyBundle\Entity\FlowException;
use DSYSurveyBundle\Repository\FlowExceptionRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * NewFlowOrEndException Validator.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class NewFlowOrEndExceptionValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($entity, Constraint $constraint)
    {
        if (!$constraint instanceof NewFlowOrEndException) {
            throw new UnexpectedTypeException($constraint, NewFlowOrEndException::class);
        }

        if (null === $entity || !$entity instanceof FlowException) {
            return;
        }

        /** @var FlowExceptionRepository $repo */
        $repo = $this->em->getRepository(FlowException::class);
        $exceptions = $repo->flowExceptions([
            'question' => $entity->getQuestion(),
            'alternative' => $entity->getAlternative(),
        ]);

        $exceptions = count(array_filter($exceptions, function (FlowException $ex) use ($entity) {
            return $ex->getId() != $entity->getId();
        }));

        $endExceptions = $repo->flowExceptions([
            'question' => $entity->getQuestion(),
            'type' => FlowException::TYPE_FINISH,
        ]);

        $endExceptions = count(array_filter($endExceptions, function (FlowException $ex) use ($entity) {
            return $ex->getId() != $entity->getId();
        }));

        if ($exceptions > 0 || $endExceptions > 0) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
