<?php

namespace DSYSurveyBundle\Validator\Constraints;

use DSYSurveyBundle\Entity\MultipleChoiceAlternativeAnswer;
use DSYSurveyBundle\Entity\Question;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * GroupChoiceValueNotNull Validator.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class GroupChoiceValueNotNullValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof GroupChoiceValueNotNull) {
            throw new UnexpectedTypeException($constraint, GroupChoiceValueNotNull::class);
        }

        if (!$value instanceof MultipleChoiceAlternativeAnswer || null === $value->getAlternative() || null === $value->getAlternative()->getQuestion()) {
            return;
        }
        $question = $value->getAlternative()->getQuestion();
        if (Question::TYPE_GROUP_CHOICE == $question->getType() && null === $value->getValue()) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
